# Tuto Experts

### Prerequisitos
Para ubuntu - Debian instalar 
```sh
$ sudo apt-get install default-libmysqlclient-dev
```

Instalar los paquetes de los cuales depende el proyecto en el virtualenv ejecutar el comando 
```sh
$ pip install -r requirements.txt
```

### Paquetes instalados
Los siguientes son los paquetes instalados y necesarios para ejecutar la aplicación de manera correcta

| Paquete | Referencia | Comando instalación |
| ------ | ------ | ------ |
| Django v1.11 | [Django] | ```pip install django==1.11 ``` |
| Mysql | [Mysql] | ``` pip install mysqlclient ``` |
| Django Settings Export | [SettingsExport] | ``` pip install django-settings-export ``` |
| Django Registration Redux | [RegistrationRedux] | ``` pip install django-registration-redux ``` |
| Pillow | [Pillow] | ``` pip install Pillow==5.2.0 ``` |
| Jet (admin theme) | [Jet] | ``` pip install django-jet ``` |
| Django Widget Tweaks | [WidgetTweaks] | ``` pip install django-widget-tweaks ``` |
| Django Math Filters | [MathFilters] | ``` pip install django-mathfilters ``` |
| Requests | [Requests] | ``` pip install requests ``` |
| Python Decouple | [Decouple] | ``` pip install python-decouple ``` |
| Redactor | [Redactor] | ``` pip install django-wysiwyg-redactor ``` |
| Unidecode | [Unidecode] | ``` pip install Unidecode ``` |
| OpenpyXL | [OpenPyXl] | ``` pip install openpyxl ``` |
| Background Tasks | [BackgroundTasks] | ``` pip install django-background-task ``` |
| Inline Css | [InlineCss] | ``` pip install django-inlinecss ``` |

### .env
Crear y configuarar archivo con las variables de entorno para el proyecto
```sh
$ cp example.env .env
```

### Datos de acceso al administrador
```sh
$ source /path_to_virtualenv/bin/activate
$ /path_to_project/django-admin createsuperuser --username admin --email admin@tutoexperts.com --password tutoAdmin123*
```
	
[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[Django]: <https://www.djangoproject.com/>
[SettingsExport]: <https://pypi.org/project/django-settings-export/>
[RegistrationRedux]: <https://django-registration-redux.readthedocs.io/en/latest/index.html>
[Pillow]: <https://pillow.readthedocs.io/en/5.2.x/index.html>
[Jet]: <http://jet.readthedocs.io/en/latest/getting_started.html>
[WidgetTweaks]: <https://pypi.org/project/django-widget-tweaks/>
[MathFilters]: <https://pypi.org/project/django-mathfilters/>
[Requests]: <https://pypi.org/project/requests/>
[Decouple]: <https://pypi.org/project/python-decouple/>
[MySql]: <https://pypi.org/project/mysqlclient/>
[Unidecode]: <https://pypi.org/project/Unidecode/>
[Redactor]: <https://pypi.org/project/django-wysiwyg-redactor/>
[OpenPyXl]: <https://pypi.org/project/openpyxl/>
[BackgroundTasks]: <https://pypi.org/project/django-background-task/>
[InlineCss]: <https://pypi.org/project/django-inlinecss/>