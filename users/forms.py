import dateutil.parser
import re
from django import forms
from datetime import datetime
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from registration.forms import RegistrationFormUniqueEmail, RegistrationFormTermsOfService
from registration.models import RegistrationProfile
from django.core.exceptions import ObjectDoesNotExist
from .models import *


class StudentSignUpForm(RegistrationFormUniqueEmail):
    """ Creación de usuarios (estudiantes) el objeto de estudiante se crea en un signal """

    terms_and_conditions = forms.BooleanField()
    phone_number = forms.IntegerField(label='Número Teléfonico (opcional)', required=False)
    preferred_subject = forms.CharField(max_length=255, required=False, label='Materia de interés (opcional)')

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get('phone_number')

        if phone_number and Student.objects.filter(phone__exact=phone_number):
            raise forms.ValidationError('Existe otro usuario con el número telefónico.')

        return phone_number

    def save_from_home(self, request):
        user = super(StudentSignUpForm, self).save(commit=False)
        user.is_active = False
        user.save()

        phone = self.cleaned_data.get('phone_number', None)
        pref_subject = self.cleaned_data.get('preferred_subject', None)
        Student.objects.create(user=user, phone=phone, preferred_subject=pref_subject)

        try:
            Subscriber.objects.get(email__iexact=user.email)
        except ObjectDoesNotExist:
            Subscriber.objects.create(email=user.email)

        reg_profile = RegistrationProfile.objects.create(user=user, **{})
        reg_profile.create_new_activation_key()
        reg_profile.send_activation_email(get_current_site(request), request)
        return user


class TeacherSignUpForm(RegistrationFormUniqueEmail):
    user_type = forms.CharField(widget=forms.HiddenInput(attrs={'value': 'teacher'}))
    first_name = forms.CharField(required=True, label='Nombres', widget=forms.TextInput(), error_messages={'required': 'Por favor ingrese su nombre'})
    last_name = forms.CharField(required=True, label='Apellidos', widget=forms.TextInput(), error_messages={'required': 'Por favor ingrese sus apellidos'})
    phone_number = forms.IntegerField(label='Número Teléfonico (opcional)', error_messages={'required': 'Por favor ingrese su número telefónico'}, required=False)
    career = forms.CharField(label='Profesión', error_messages={'required': 'Por favor ingrese su profesión'})
    id_number = forms.IntegerField(label='Número de identificación', widget=forms.NumberInput(), error_messages={'required': 'Por favor ingrese su número de identificacion'})
    certificate = forms.FileField(label='Diploma (opcional)', widget=forms.FileInput(), required=False)
    juditial_past = forms.FileField(error_messages={'required': 'Por favor adjunte sus antecedentes penales'}, label='Antecedentes Judiciales', widget=forms.FileInput(), help_text='Puede consultar sus antecedentes judiciales <a href="https://antecedentes.policia.gov.co:7005/WebJudicial/index.xhtml" target="_blank">Aquí</a>')
    photo = forms.FileField(label='Foto', required=True, widget=forms.FileInput(), error_messages={'required': 'La foto es requerida'})
    areas = forms.ModelMultipleChoiceField(queryset=Area.objects.all().filter(active=True, area_subjects__active=True).annotate(Count('id')).order_by('level'), required=False)
    subjects = forms.ModelMultipleChoiceField(queryset=Subject.objects.filter(active=True).order_by('area__level'), required=False)
    days = forms.MultipleChoiceField(choices=DAYS_OF_WEEK_CHOICES, required=False)
    skype_user = forms.CharField(label='Usuario de Skype', required=True, error_messages={'required': 'Por favor ingrese su nombre de usuario de skype'})
    work_mode = forms.ChoiceField(choices=CLASS_MODE_CHOICES, required=True, label='Modo de trabajo', error_messages={'required': 'Por favor seleccione al menos una modalidad de trabajo'})
    terms_and_conditions = forms.BooleanField(error_messages={'required': 'Debe aceptar los términos y condiciones.'})

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(TeacherSignUpForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = ('username', 'email')

    def clean_id_number(self):
        id_number = self.cleaned_data.get('id_number')
        if Teacher.objects.filter(id_number__exact=id_number):
            raise forms.ValidationError(
                'Existe otro usuario con el número de identificaión.')
        return id_number

    def clean_phone_number(self):
        phone_number = self.cleaned_data.get('phone_number')
        if phone_number and Teacher.objects.filter(phone__exact=phone_number):
            raise forms.ValidationError('Existe otro usuario con el número telefónico.')

        return phone_number

    def clean_subjects(self):
        subjects = self.cleaned_data.get('subjects')

        if len(subjects) == 0:
            raise forms.ValidationError('Debe seleccionar al menos un area de trabajo')

        return subjects

    def clean_days(self):
        days = self.cleaned_data.get('days')
        days_start_time = self.request.POST.getlist('schedule_start', [])
        days_end_time = self.request.POST.getlist('schedule_end', [])

        if len(days) == 0:
            raise forms.ValidationError('Debe seleccionar al menos un día de trabajo')

        for i, day in enumerate(days):
            day = int(day)
            day_name = DAYS_OF_WEEK_CHOICES[day - 1][1]
            start_time = days_start_time[i]
            end_time = days_end_time[i]

            if not start_time:
                raise forms.ValidationError('No ha seleccionado hora de inicio de atención para el día ' + day_name)

            if not end_time:
                raise forms.ValidationError('No ha seleccionado hora de finalización de atención para el día ' + day_name)

            if start_time == end_time:
                raise forms.ValidationError('Seleccione un horario de atención válido para el día ' + day_name)

        return days

    def clean_work_mode(self):
        work_mode = self.cleaned_data.get('work_mode')
        if len(work_mode) < 1:
            raise forms.ValidationError('Seleccione al menos una modalidad de trabajo')
        return work_mode

    def save(self, commit=True):
        user_instance = super(TeacherSignUpForm, self).save(commit=False)
        user_instance.first_name = self.cleaned_data.get('first_name')
        user_instance.last_name = self.cleaned_data.get('last_name')
        user_instance.is_active = False
        user_instance.save()

        reg_profile = RegistrationProfile.objects.create(user=user_instance, **{})
        reg_profile.create_new_activation_key()

        teacher = Teacher(user=user_instance)
        teacher.id_number = self.cleaned_data.get('id_number')
        teacher.career = self.cleaned_data.get('career')
        teacher.phone = self.cleaned_data.get('phone_number')
        teacher.certificate = self.cleaned_data.get('certificate')
        teacher.juditial_past = self.cleaned_data.get('juditial_past')
        teacher.photo = self.cleaned_data.get('photo')
        teacher.work_mode = self.cleaned_data.get('work_mode')
        teacher.skype_user = self.cleaned_data.get('skype_user')
        teacher.save()

        subjects = self.cleaned_data.get('subjects', [])

        days = self.cleaned_data.get('days')
        days_start_time = self.request.POST.getlist('schedule_start', [])
        days_end_time = self.request.POST.getlist('schedule_end', [])

        for subject in subjects:
            price = self.request.POST.get('skill_subject_price_' + str(subject.pk))
            Skill.objects.create(teacher=user_instance, area=subject.area, subject=subject, price=price)

        for i, day in enumerate(days):
            day = int(day)
            start_time = dateutil.parser.parse(days_start_time[i])
            end_time = dateutil.parser.parse(days_end_time[i])

            Schedule.objects.create(teacher=user_instance, day=day, start_time=start_time, end_time=end_time)

        try:
            Subscriber.objects.get(email__iexact=user_instance.email)
        except ObjectDoesNotExist:
            Subscriber.objects.create(email=user_instance.email)

        reg_profile.send_activation_email(get_current_site(self.request), self.request)
        return user_instance


class TeacherUpdateForm(forms.ModelForm):
    phone = forms.IntegerField(label='Número Teléfonico', error_messages={'required': 'Por favor digite su número telefónico'}, required=False)
    career = forms.CharField(label='Profesión', error_messages={'required': 'Por favor digite su profesión'})
    id_number = forms.IntegerField(label='Número de identificaión', widget=forms.NumberInput(), error_messages={'required': 'Por favor digite su número de identificacion'})
    bio = forms.CharField(label='Acerca de mí', error_messages={'required': 'Cuentanos un poco más acerca de ti, completando la biografía'}, widget=forms.Textarea(attrs={'rows': '5'}))
    certificate = forms.FileField(label='Diploma', required=False, widget=forms.FileInput())
    juditial_past = forms.FileField(label='Pasado Judicial', required=False, widget=forms.FileInput(), help_text='Puede consultar sus antecedentes judiciales <a href="https://antecedentes.policia.gov.co:7005/WebJudicial/index.xhtml" target="_blank">Aquí</a>')
    photo = forms.FileField(label='Foto', required=False, widget=forms.FileInput())
    skype_user = forms.CharField(label='Skype', required=True)
    facebook_link = forms.CharField(label='Facebook', required=False, widget=forms.URLInput)
    instagram_link = forms.CharField(label='Instagram', required=False, widget=forms.URLInput)
    twitter_link = forms.CharField(label='Twitter', required=False, widget=forms.URLInput)
    linkedin_link = forms.CharField(label='LinkedIn', required=False, widget=forms.URLInput)
    google_link = forms.CharField(label='Google ', required=False, widget=forms.URLInput)
    areas = forms.ModelMultipleChoiceField(queryset=Area.objects.all().filter(active=True, area_subjects__active=True).annotate(Count('id')).order_by('level'), required=False)
    subjects = forms.ModelMultipleChoiceField(queryset=Subject.objects.filter(active=True).order_by('area__level'), required=False)
    days = forms.MultipleChoiceField(choices=DAYS_OF_WEEK_CHOICES, required=False)
    work_mode = forms.ChoiceField(choices=CLASS_MODE_CHOICES, required=True, label='Modo de trabajo', error_messages={'required': 'Por favor seleccione al menos una modalidad de trabajo'})

    class Meta:
        model = User
        fields = ('first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(TeacherUpdateForm, self).__init__(*args, **kwargs)

    def clean_bio(self):
        return self.cleaned_data.get('bio', '').strip().lstrip()

    def clean_subjects(self):
        subjects = self.cleaned_data.get('subjects')

        if len(subjects) == 0:
            raise forms.ValidationError('Debe seleccionar al menos un area de trabajo')

        return subjects

    def clean_days(self):
        days = self.cleaned_data.get('days')
        days_start_time = self.request.POST.getlist('schedule_start', [])
        days_end_time = self.request.POST.getlist('schedule_end', [])

        if len(days) == 0:
            raise forms.ValidationError('Debe seleccionar al menos un día de trabajo')

        for i, day in enumerate(days):
            day = int(day)
            day_name = DAYS_OF_WEEK_CHOICES[day - 1][1]
            start_time = days_start_time[i]
            end_time = days_end_time[i]

            if not start_time:
                raise forms.ValidationError('No ha seleccionado hora de inicio de atención para el día ' + day_name)

            if not end_time:
                raise forms.ValidationError('No ha seleccionado hora de finalización de atención para el día ' + day_name)

            if start_time == end_time:
                raise forms.ValidationError('Seleccione un horario de atención válido para el día ' + day_name)

        return days


class StudentUpdateForm(forms.ModelForm):
    photo = forms.FileField(label='Foto', required=False, widget=forms.FileInput())
    phone_number = forms.IntegerField(label='Número Teléfonico', required=False)
    preferred_subject = forms.CharField(max_length=255, required=False, label='Materia de interés')
    birth_date = forms.DateField(label='Fecha de nacimimento', required=False, input_formats=['%Y-%m-%d'],
                                 widget=forms.DateInput(attrs={'class': 'sm-form-control required datetimepicker', 'placeholder': 'YYYY-MM-DD'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(StudentUpdateForm, self).__init__(*args, **kwargs)


class SearchTeacherForm(forms.Form):
    area = forms.ModelChoiceField(queryset=Area.objects.filter(active=True), widget=forms.Select(attrs={'class': 'sm-form-control not-dark'}))
    subject = forms.ModelChoiceField(label='Materia', queryset=Subject.objects.none(), widget=forms.Select(attrs={'class': 'sm-form-control not-dark'}))
    class_mode = forms.ChoiceField(label='Modalidad', choices=CLASS_MODE_CHOICES_FOR_SEARCH, widget=forms.Select(attrs={'class': 'sm-form-control not-dark'}))
    class_date = forms.DateField(label='Fecha', input_formats=['%Y-%m-%d'], widget=forms.DateInput(attrs={'class': 'sm-form-control required datetimepicker', 'placeholder': 'YYYY-MM-DD', 'autocomplete': 'off'}))
    class_time = forms.CharField(label='Hora', widget=forms.TextInput(attrs={'class': 'sm-form-control required timepicker', 'placeholder': 'HH:MM', 'autocomplete': 'off'}))
    duration = forms.IntegerField(label='Duración')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['subject'].queryset = Subject.objects.none()

        if 'area' in self.data:
            try:
                area_id = int(self.data.get('area'))
                self.fields['subject'].queryset = Subject.objects.filter(
                    area_id=area_id).order_by('name')
            except (ValueError, TypeError):
                pass


class CheckOutForm(forms.ModelForm):
    payment_method = forms.CharField(max_length=10)
    first_name = forms.CharField(required=True, label='Nombres', widget=forms.TextInput(), error_messages={'required': 'Por favor digite su nombre'})
    last_name = forms.CharField(required=True, label='Apellidos', widget=forms.TextInput(), error_messages={'required': 'Por favor digite sus apellidos'})
    city = forms.CharField(required=True, label='Cuidad', widget=forms.TextInput(), error_messages={'required': 'Por favor digite el nombre de la cuidad donde reside'})
    address = forms.CharField(required=True, label='Dirección', widget=forms.TextInput(), error_messages={'required': 'Por favor digite su dirección'})
    phone_number = forms.CharField(label='Número Teléfonico', error_messages={'required': 'Por favor digite su número telefónico'})
    email = forms.EmailField(label='Correo Eléctronico', widget=forms.EmailInput(), error_messages={'required': 'Por favor digite su dirección de correo eléctronico'})
    pse_payer_name = forms.CharField(max_length=250, required=False, label='Nombre del titular')
    pse_payer_type = forms.ChoiceField(choices=PSE_CUSTOMER_TYPE, required=False, label='Tipo de cliente')
    pse_payer_id_type = forms.ChoiceField(choices=PSE_CUSTOMER_ID_TYPE, required=False, label='Tipo de Documento')
    pse_payer_id = forms.CharField(required=False, label='Número de identificación')
    pse_payer_phone = forms.CharField(required=False, label='Teléfono')
    cc_number = forms.CharField(required=False, label='Número de tarjeta', help_text='número de tarjeta sin guiones o espacios')
    cc_titular = forms.CharField(required=False, label='Titular de la tarjeta', max_length=30, help_text='como aparece en su tarjeta')
    cc_titular_id = forms.CharField(required=False, label='Número de identificación')
    cc_expiration_month = forms.CharField(required=False, label='Fecha de caducidad', max_length=2)
    cc_expiration_year = forms.CharField(required=False, max_length=6)
    cc_verification_code = forms.IntegerField(label='Código de verificación', required=False)

    class Meta:
        model = Reservation
        fields = ('topic', )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(CheckOutForm, self).__init__(*args, **kwargs)

    def check_only_number(self, target):
        pattern = re.compile('\d+$')

        if pattern.match(str(target)):
            return True

        return False

    def is_luhn_valid(self, cc):
        num = list(map(int, str(cc)))
        return sum(num[::-2] + [sum(divmod(d * 2, 10)) for d in num[-2::-2]]) % 10 == 0

    def get_cc_franchise(self, cc):
        cc_number = str(cc)

        visa_pattern = re.compile('^(4)(\d{12}|\d{15})$|^(606374\d{10}$)')
        master_card_pattern = re.compile('^(5[1-5]\d{14}$)|^(2(?:2(?:2[1-9]|[3-9]\d|[3-6]\d\d|7(?:[01]\d|20)))\d{12}$)')
        amex_pattern = re.compile('^(3[47]\d{13})$')
        diners_pattern = re.compile('(^[35](?:0[0-5]|[68][0-9])[0-9]{11}$)|(^30[0-5]{11}$)|(^3095(\d{10})$)|(^36{12}$)|(^3[89](\d{12})$)')
        codensa_pattern = re.compile('^590712(\d{10})$')

        if visa_pattern.match(cc_number):
            return 'VISA'
        if master_card_pattern.match(cc_number):
            return 'MASTERCARD'
        if amex_pattern.match(cc_number):
            return 'AMEX'
        if diners_pattern.match(cc_number):
            return 'DINERS'
        if codensa_pattern.match(cc_number):
            return 'CODENSA'

        raise Exception('Su pago no se pudo procesar debido a que su tarjeta no pertenece a ninguna de las siguientes franquicias VISA, MASTER CARD, AMEX, DINERS y CODENSA.')

    def clean_cc_number(self):
        cc_number = self.cleaned_data.get('cc_number')

        if cc_number and self.check_only_number(target=cc_number):
            pattern = re.compile('^\d{13,16}$')

            if not pattern.match(cc_number):
                raise forms.ValidationError('Número de tarjeta inválido, por favor rectifique (solo debe contener números).')

            if not self.is_luhn_valid(cc=cc_number):
                raise forms.ValidationError('Número de tarjeta inválido, por favor rectifique y vuelva a intentar.')

        return cc_number

    def clean_cc_verification_code(self):
        vc = self.cleaned_data.get('cc_verification_code')

        if vc and not self.check_only_number(vc):
            raise forms.ValidationError('Código de verificación inválido, por favor rectifique (solo debe contener números).')

        return vc

    def clean_cc_titular(self):
        name = self.cleaned_data.get('cc_titular')
        pattern = re.compile('^[a-zA-Z\s]+$')

        if name and not pattern.match(name):
            raise forms.ValidationError('El nombre del titular debe contener solo letras (sin acentos) y espacios, tal como aparece en su tarjeta')

        return name

    def clean_payment_method(self):
        pm = self.cleaned_data.get('payment_method')

        if pm not in ('BALOTO', 'EFECTY', 'PSE', 'TC'):
            raise forms.ValidationError('Método de pago inválido.')

        if pm == 'TC':
            cc_number = self.request.POST.get('cc_number')
            cc_titular = self.request.POST.get('cc_titular')
            cc_verification = self.request.POST.get('cc_verification_code')
            cc_expiration_month = self.request.POST.get('cc_expiration_month')
            cc_expiration_year = self.request.POST.get('cc_expiration_year')

            if not cc_number or not cc_titular or not cc_verification or not cc_expiration_month or not cc_expiration_year:
                raise forms.ValidationError('Por favor digite todos los campos para realizar el pago con su tarjeta de crédito.')

            today = datetime.today()
            try:
                if int(str(cc_expiration_year)) < today.year or (int(str(cc_expiration_month)) < today.month and int(str(cc_expiration_year)) == today.year):
                    raise forms.ValidationError('La fecha de caducidad no puede ser menor a la fecha actual.')
            except ValueError:
                raise forms.ValidationError('La fecha de caducidad no es válida.')

            try:
                pm = self.get_cc_franchise(cc=cc_number)
            except Exception as error:
                raise forms.ValidationError(str(error))

            if pm == 'AMEX' and len(str(cc_verification)) != 4:
                raise forms.ValidationError('Código de verificación inválido, por favor rectifique.')

        return pm


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ('rating', 'text')
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control form-control-lg required', 'rows': 4, 'cols': 30, 'placeholder': 'Comparte tu experiencia', 'minlength': 20})
        }


class PaymentForm(forms.ModelForm):
    confirmation_file = forms.FileField(
        label='Confirmación de pagos',
        help_text='Archivo excel con listado de confirmacion de pagos realizados',
        widget=forms.FileInput(attrs={'accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'})
    )

    class Meta:
        model = Payment
        fields = '__all__'
