import hashlib
import json
import os
import time
import urllib
from datetime import datetime, timedelta

import dateutil.parser
import requests
from django import forms
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ObjectDoesNotExist
from django.core.files import File
from django.core.signing import Signer
from django.db.models import Q, Sum
from django.forms.utils import ErrorList
from django.http import (HttpResponse, HttpResponseNotAllowed,
                         HttpResponseNotFound, HttpResponseRedirect,
                         JsonResponse)
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from registration.backends.default.views import RegistrationView

from theme.models import GeneralSetting
from tuto_experts import utils as tuto_utils

from .forms import *
from .models import *
from .tasks import SendStudentReservationConfirmEmail

# Create your views here.


class TeacherRegistrationView(generic.edit.FormView):
    template_name = 'registration/teacher_registration_form.html'
    form_class = TeacherSignUpForm
    success_url = reverse_lazy('registration_complete')

    def get_form_kwargs(self):
        kwargs = super(TeacherRegistrationView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        form.request = self.request
        try:
            form.save()
            return redirect(self.success_url)
        except Exception as ex:
            return redirect('teacher_signup')


@method_decorator(login_required, name='dispatch')
class UserProfileView(generic.DetailView, generic.list.MultipleObjectMixin, generic.edit.FormMixin):
    model = User
    template_name = 'users/user_profile.html'
    paginate_by = 5
    form_class = FeedbackForm

    def get_success_url(self):
        return reverse_lazy('users:user_profile', kwargs={'pk': self.get_object().pk})

    def get_context_data(self, **kwargs):
        is_teacher, skills, total_money = False, [], 0
        try:
            self.object.teacher
            reservations = self.object.teacher_reservations.filter(status='Transacción aprobada')
            total_money = reservations.filter(paid=False).aggregate(Sum('amount_to_paid'))['amount_to_paid__sum'] or 0.00
            skills = self.object.skills.filter(active=True)
            is_teacher = True
        except ObjectDoesNotExist as ex:
            reservations = self.object.student_reservations.filter(status__in=['Transacción aprobada', 'Pendiente'])

        self.object_list = reservations
        context = super(UserProfileView, self).get_context_data(is_teacher=is_teacher, skills=skills, total_money=total_money, ** kwargs)
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        feedback = form.save(commit=False)
        feedback.user = self.get_object()
        feedback.save()

        messages.add_message(self.request, messages.SUCCESS, 'Muchas gracias por tus comentarios!', 'icon-thumbs-up')
        return redirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class TeacherProfileUpdateView(generic.edit.UpdateView):
    form_class = TeacherUpdateForm
    template_name = 'users/teacher_update_form.html'

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse_lazy('users:user_profile', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(TeacherProfileUpdateView, self).get_context_data(**kwargs)
        context['skills'] = list(self.object.skills.filter(active=True).values('subject', 'price'))
        context['schedule'] = list(self.object.schedules.all().values('day', 'start_time', 'end_time'))
        return context

    def get_form_kwargs(self):
        kwargs = super(TeacherProfileUpdateView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        user_instance = self.object
        teacher = user_instance.teacher
        teacher.biography = form.cleaned_data.get('bio')
        teacher.phone = form.cleaned_data.get('phone')
        teacher.career = form.cleaned_data.get('career')
        teacher.id_number = form.cleaned_data.get('id_number')
        teacher.work_mode = form.cleaned_data.get('work_mode')
        teacher.skype_user = form.cleaned_data.get('skype_user')
        teacher.facebook_link = form.cleaned_data.get('facebook_link')
        teacher.instagram_link = form.cleaned_data.get('instagram_link')
        teacher.twitter_link = form.cleaned_data.get('twitter_link')
        teacher.linkedin_link = form.cleaned_data.get('linkedin_link')
        teacher.google_link = form.cleaned_data.get('google_link')

        if self.request.FILES.get('photo'):
            teacher.photo = form.cleaned_data.get('photo')

        if self.request.FILES.get('certificate'):
            teacher.certificate = form.cleaned_data.get('certificate')

        if self.request.FILES.get('juditial_past'):
            teacher.juditial_past = form.cleaned_data.get('juditial_past')

        subjects = form.cleaned_data.get('subjects', [])

        days = form.cleaned_data.get('days', [])
        days_start_time = self.request.POST.getlist('schedule_start', [])
        days_end_time = self.request.POST.getlist('schedule_end', [])

        teacher_skills = user_instance.skills.all().update(active=False)  # Deshabilito las anteriores y registro las nuevas
        user_instance.schedules.all().delete()

        for subject in subjects:
            price = self.request.POST.get('skill_subject_price_' + str(subject.pk))

            try:
                skill = Skill.objects.get(teacher=user_instance, area=subject.area, subject=subject, active=False)
                skill.price = price
                skill.active = True
                skill.save()
            except Skill.DoesNotExist:
                Skill.objects.create(teacher=user_instance, area=subject.area, subject=subject, price=price)

        for i, day in enumerate(days):
            day = int(day)
            start_time = dateutil.parser.parse(days_start_time[i])
            end_time = dateutil.parser.parse(days_end_time[i])

            Schedule.objects.create(teacher=user_instance, day=day, start_time=start_time, end_time=end_time)

        teacher.save()
        user_instance = form.save()
        return redirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class StudentProfileUpdateView(generic.edit.UpdateView):
    form_class = StudentUpdateForm
    template_name = 'users/student_update_form.html'

    def get_success_url(self):
        return reverse_lazy('users:user_profile', kwargs={'pk': self.object.pk})

    def get_object(self, queryset=None):
        return self.request.user

    def get_form_kwargs(self):
        kwargs = super(StudentProfileUpdateView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        user_instance = self.object
        student = user_instance.student

        if self.request.POST.get('birth_date'):
            student.birth_date = self.request.POST.get('birth_date')

        if self.request.FILES.get('photo'):
            student.photo = self.request.FILES.get('photo')

        if self.request.POST.get('phone_number'):
            student.phone = self.request.POST.get('phone_number')

        if self.request.POST.get('preferred_subject'):
            student.preferred_subject = self.request.POST.get('preferred_subject')

        student.save()
        user_instance = form.save()
        return redirect(self.get_success_url())


@method_decorator(login_required, name='dispatch')
class SearchTeacherView(generic.edit.FormView):
    template_name = 'users/search_teacher.html'
    form_class = SearchTeacherForm

    def form_valid(self, form):
        context = self.get_context_data()
        form_data = form.cleaned_data

        string_date = '%s %s' % (form_data.get('class_date'), form_data.get('class_time'))
        class_date_time = datetime.strptime(string_date, '%Y-%m-%d %I:%M %p')
        week_day = 1 if class_date_time.weekday() == 0 else class_date_time.weekday() + 1
        start_time = class_date_time.time()
        end_time = (datetime.strptime(string_date, '%Y-%m-%d %I:%M %p') + timedelta(hours=form_data.get('duration'))).time()

        search_params = {
            'subject': form_data.get('subject').pk,
            'area': form_data.get('area').pk,
            'class_mode': form_data.get('class_mode'),
            'week_day': week_day,
            'start_time': start_time,
            'end_time': end_time
        }

        self.request.session['reservation_subject'] = form_data.get('subject').pk
        self.request.session['reservation_mode'] = form_data.get('class_mode')
        self.request.session['reservation_duration'] = form_data.get('duration')
        self.request.session['reservation_date'] = string_date

        return redirect(reverse('users:teacher_result') + '?' + urllib.parse.urlencode(search_params))


@method_decorator(login_required, name='dispatch')
class TeacherResultView(generic.ListView):
    model = Skill
    template_name = 'users/teachers_result.html'
    context_object_name = 'skills'

    def get_queryset(self):
        query_params = self.request.GET
        try:
            return Skill.objects.filter(active=True,
                                        subject_id=query_params['subject'],
                                        area_id=query_params['area'],
                                        teacher__teacher__work_mode__in=[query_params['class_mode'], CLASS_MODE_CHOICES.__getitem__(2)[0]],
                                        teacher__schedules__day=query_params['week_day'],
                                        teacher__schedules__start_time__lte=query_params['start_time'],
                                        teacher__schedules__end_time__gte=query_params['end_time']).order_by('-teacher__teacher__rating')
        except Exception as ex:
            return None


@method_decorator(login_required, name='dispatch')
class TeacherDetailView(generic.DetailView):
    model = Teacher
    template_name = 'users/teacher_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TeacherDetailView, self).get_context_data(**kwargs)
        reservations = checkTeacherAviability(self.request.session['reservation_date'], self.request.session['reservation_duration'], self.object.user)

        context['skills'] = self.object.user.skills.filter(active=True)
        context['has_reservation'] = True if reservations else False
        self.request.session['reservation_teacher'] = self.object.pk
        return context


@method_decorator([login_required, never_cache], name='dispatch')
class CheckoutView(generic.edit.FormView):
    template_name = 'payments/checkout.html'
    form_class = CheckOutForm
    success_url = reverse_lazy('home')
    default_timeout = 60

    def get_context_data(self, **kwargs):
        context = super(CheckoutView, self).get_context_data(**kwargs)

        teacher = Teacher.objects.get(pk=self.request.session['reservation_teacher'])
        subject = Subject.objects.get(pk=self.request.session['reservation_subject'])
        skill = teacher.user.skills.get(subject=subject)
        class_duration = self.request.session['reservation_duration']

        payu = Payu()
        bank_data = payu.get_bank_list_json()
        base_session = '%s%s' % (self.request.COOKIES['sessionid'], int(round(time.time() * 1000)))
        session_id = hashlib.md5(base_session.encode('utf-8'))

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'Content-Length': str(len(json.dumps(bank_data)))
        }
        bank_list_request = requests.post('https://api.payulatam.com/payments-api/4.0/service.cgi', data=json.dumps(bank_data), headers=headers, timeout=self.default_timeout)
        bank_list_response = bank_list_request.json()

        context['teacher'] = teacher
        context['skill'] = skill
        context['subject'] = subject
        context['tax_price'] = (int(class_duration) * skill.price) * settings.TAX_BASE
        context['banks'] = bank_list_response.get('banks')
        context['session_id'] = session_id.hexdigest()

        return context

    def get_form_kwargs(self):
        kwargs = super(CheckoutView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_success_url(self, reservation):
        return reverse_lazy('users:reservation', kwargs={'pk': reservation.pk})

    def form_valid(self, form):
        context_data = self.get_context_data()
        teacher = context_data['teacher']
        skill = context_data['skill']
        subject = context_data['subject']
        duration = self.request.session['reservation_duration']
        meta_data = get_request_meta(self.request)

        # Validar si el docente tiene reservas para que no entre en conflicto
        reservations = checkTeacherAviability(self.request.session['reservation_date'], duration, teacher.user)
        if reservations:
            form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                'Lo sentimos, el docente ya tiene una reserva asignada.'
            ])
            return self.form_invalid(form)

        payu = Payu()
        payu.email = form.cleaned_data.get('email')
        payu.full_name = '%s %s' % (form.cleaned_data.get('first_name'), form.cleaned_data.get('last_name'))
        payu.base_price = duration * skill.price
        payu.phone = form.cleaned_data.get('phone_number')
        payu.address = form.cleaned_data.get('address')
        payu.city = form.cleaned_data.get('city')
        payu.description = form.cleaned_data.get('topic')[:250]
        payu.client_ip = meta_data.get('remoteAddress')
        payu.user_agent = meta_data.get('userAgent')
        payu.student_id = self.request.user.id
        payu.device_session_id = context_data['session_id']
        payu.payment_method = form.cleaned_data.get('payment_method')

        if payu.payment_method in ['BALOTO', 'EFECTY']:
            data = payu.get_cash_payment_json()
        elif payu.payment_method == 'PSE':
            payu.payer_name = form.cleaned_data.get('pse_payer_name')
            payu.payer_phone = form.cleaned_data.get('pse_payer_phone')
            payu.payer_type = form.cleaned_data.get('pse_payer_type')
            payu.payer_id_type = form.cleaned_data.get('pse_payer_id_type')
            payu.payer_id = form.cleaned_data.get('pse_payer_id')
            payu.bank_id = self.request.POST.get('pse_bank_id')
            data = payu.get_pse_payment_json()
        elif payu.payment_method in ['VISA', 'MASTERCARD', 'AMEX', 'DINERS', 'CODENSA']:
            payu.payer_name = form.cleaned_data.get('cc_titular')
            payu.payer_id = form.cleaned_data.get('cc_titular_id')
            payu.cc_number = form.cleaned_data.get('cc_number')
            payu.cc_verfication_code = form.cleaned_data.get('cc_verification_code')
            payu.cc_expiration = '%s/%s' % (form.cleaned_data.get('cc_expiration_year'), form.cleaned_data.get('cc_expiration_month'))
            data = payu.get_cc_payment_json()

        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'Content-Length': str(len(json.dumps(data)))
        }

        payu_request = requests.post('https://api.payulatam.com/payments-api/4.0/service.cgi', data=json.dumps(data), headers=headers, timeout=self.default_timeout)
        response = payu_request.json()

        if response.get('code') != 'SUCCESS' or response.get('error') or response.get('transactionResponse').get('state') == 'DECLINED':
            if response.get('error'):
                form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                    u'Transacción declinada [%s]' % response.get('error', '001')
                ])
            else:
                form._errors[forms.forms.NON_FIELD_ERRORS] = ErrorList([
                    u'Transacción declinada [%s]' % response.get('transactionResponse').get('responseCode', '001')
                ])
            return self.form_invalid(form)

        total_price = data.get('transaction').get('order').get('additionalValues').get('TX_VALUE').get('value')

        reservation = Reservation()
        reservation.student = self.request.user
        reservation.teacher = teacher.user
        reservation.subject = subject
        reservation.topic = form.cleaned_data.get('topic')
        reservation.duration = duration
        reservation.start_datetime_class = datetime.strptime(self.request.session['reservation_date'], '%Y-%m-%d %I:%M %p')
        reservation.mode = self.request.session['reservation_mode']
        reservation.price = total_price
        reservation.address = payu.address
        reservation.reference_code = data.get('transaction').get('order').get('referenceCode')
        reservation.order_id = response.get('transactionResponse').get('orderId')
        reservation.transaction_id = response.get('transactionResponse').get('transactionId')
        reservation.trazability_code = response.get('transactionResponse').get('trazabilityCode')

        if response.get('transactionResponse').get('authorizationCode'):
            reservation.authorization_code = response.get('transactionResponse').get('authorizationCode')

        if payu.payment_method in ['BALOTO', 'EFECTY']:
            reservation.url_payment_pdf = response.get('transactionResponse').get('extraParameters').get('URL_PAYMENT_RECEIPT_PDF')
            reservation.url_payment_web = response.get('transactionResponse').get('extraParameters').get('URL_PAYMENT_RECEIPT_HTML')
            reservation.reference_number = response.get('transactionResponse').get('extraParameters').get('REFERENCE')

        reservation.save()

        if payu.payment_method in ['BALOTO', 'EFECTY']:
            return redirect(reservation.url_payment_web)
        if payu.payment_method == 'PSE':
            return redirect(response.get('transactionResponse').get('extraParameters').get('BANK_URL'))

        return redirect(self.get_success_url(reservation=reservation))


@method_decorator(login_required, name='dispatch')
class ReservationDetailView(generic.DetailView):
    model = Reservation
    template_name = 'payments/reservation_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ReservationDetailView, self).get_context_data(**kwargs)
        context['payu_information'] = self.object.get_payu_information()

        try:
            self.request.user.teacher
            context['is_teacher'] = True
        except ObjectDoesNotExist as ex:
            context['is_teacher'] = False

        return context


@method_decorator([login_required, never_cache], name='dispatch')
class PaymentResponseView(generic.DetailView):
    template_name = 'payments/payment_response.html'

    def get_object(self):
        reference_code = self.request.GET.get('referenceCode', '')
        return get_object_or_404(Reservation, reference_code=reference_code)

    def get_context_data(self, **kwargs):
        context = super(PaymentResponseView, self).get_context_data(**kwargs)
        pol_transaction_state = int(self.request.GET.get('polTransactionState'))
        pol_response_code = int(self.request.GET.get('polResponseCode'))
        context['reservation_status'] = 'Error'

        if pol_transaction_state == 4 and pol_response_code == 1:
            context['reservation_status'] = 'Transacción Aprobada'
        if pol_transaction_state == 6 and pol_response_code == 4:
            context['reservation_status'] = 'Transacción Rechazada'
        if pol_transaction_state == 6 and pol_response_code == 5:
            context['reservation_status'] = 'Transacción Fallida'
        if pol_transaction_state in [12, 14] and pol_response_code in [25, 9994]:
            context['reservation_status'] = 'Transacción pendiente, por favor revisar si el débito fue realizado en el banco'

        return context


@csrf_exempt
def payment_confirmation(request):
    if request.method == 'POST':
        transaction_id = request.POST.get('transaction_id')
        reference_code = request.POST.get('reference_sale')

        try:
            reservation = Reservation.objects.get(reference_code=reference_code)
            response_code_pol = int(request.POST.get('response_code_pol'))

            if response_code_pol == 1:
                status = 'Transacción aprobada'
            elif response_code_pol == 4:
                status = 'Transacción rechazada por entidad financiera'
            elif response_code_pol == 5:
                status = 'Transacción rechazada por el banco'
            elif response_code_pol == 6:
                status = 'Fondos insuficientes'
            elif response_code_pol == 7:
                status = 'Tarjeta inválida'
            elif response_code_pol == 8:
                status = 'Contactar entidad financiera'
            elif response_code_pol == 9:
                status = 'Tarjeta vencida'
            elif response_code_pol == 10:
                status = 'Tarjeta restringida'
            elif response_code_pol == 12:
                status = 'Fecha de expiración o código de seguridadinválidos'
            elif response_code_pol == 13:
                status = 'Reintentar pago'
            elif response_code_pol == 14:
                status = 'Transacción inválida'
            elif response_code_pol == 17:
                status = 'El valor excede el máximo permitido por la entidad'
            elif response_code_pol == 19:
                status = 'Transacción abandonada por el pagador'
            elif response_code_pol == 20:
                status = 'Transacción expirada'
            elif response_code_pol == 22:
                status = 'Tarjeta no autorizada para comprar por internet'
            elif response_code_pol == 23:
                status = 'Transacción rechazada por sospecha de fraude'
            elif response_code_pol == 9995:
                status = 'Certificado digital no encontrado'
            elif response_code_pol == 9996:
                status = 'Error tratando de comunicarse con el banco'
            elif response_code_pol == 9997:
                status = 'Error comunicándose con la entidad financiera'
            elif response_code_pol == 9998:
                status = 'Transacción no permitida'
            elif response_code_pol == 9999:
                status = 'Error'

            reservation.status = status
            reservation.transaction_id = transaction_id
            reservation.save()

            if response_code_pol == 1:
                try:
                    notification_time = int(GeneralSetting.objects.get(key__exact='rsv_cnfrm_ntf_time').value)
                except GeneralSetting.DoesNotExist:
                    notification_time = 15

                try:
                    bcc_list = GeneralSetting.objects.get(key__exact='bcc_reservation_list').value
                except GeneralSetting.DoesNotExist:
                    bcc_list = None

                email_data = {
                    'reservation': reservation,
                    'scheme': request.scheme,
                    'host': request.get_host(),
                    'site_name': get_current_site(request).name,
                    'is_confirmation': False
                }

                tuto_utils.send_templated_email(
                    subject='Su reserva ha sido programada',
                    recipients=reservation.student.email,
                    template_path='theme/email/student_reservation_notification.html',
                    data=email_data,
                    hidden_recipients=bcc_list
                )

                tuto_utils.send_templated_email(
                    subject='Nueva reserva',
                    recipients=reservation.teacher.email,
                    template_path='theme/email/teacher_reservation_notification.html',
                    data=email_data,
                    hidden_recipients=bcc_list
                )

                SendStudentReservationConfirmEmail(
                    reservation_id=reservation.id,
                    scheme=request.scheme,
                    host=request.get_host(),
                    site_name=get_current_site(request).name,
                    schedule=reservation.start_datetime_class - timedelta(minutes=notification_time)
                )

            return HttpResponse('Operación exitosa')
        except Reservation.DoesNotExist:
            return HttpResponseNotFound('No se pudo completar la operación [404]')

    return HttpResponseNotAllowed('Operación no permitida')


class ReservationConfirmationView(generic.RedirectView):
    permanent = False
    query_string = True
    pattern_name = 'reservation-detail'

    def get_redirect_url(self, *args, **kwargs):
        reservation = get_object_or_404(Reservation, reference_code=kwargs['reservation_code'])

        if not reservation.confirmed:
            reservation.confirmed = True
            reservation.save()
            # Enviar correo al docente
            tuto_utils.send_templated_email(
                subject='Confirmación de reserva',
                recipients=reservation.teacher.email,
                template_path='theme/email/teacher_reservation_notification.html',
                data={
                    'reservation': reservation,
                    'request': self.request,
                    'site_name': get_current_site(self.request).name,
                    'is_confirmation': True
                }
            )

        return super(ReservationConfirmationView, self).get_redirect_url(*args, **kwargs)


class ReservationResumeDetailView(generic.DetailView):
    model = Reservation
    template_name = 'payments/reservation_detail.html'
    slug_field = 'reference_code'
    slug_url_kwarg = 'reservation_code'

    def get_context_data(self, **kwargs):
        context = super(ReservationResumeDetailView, self).get_context_data(**kwargs)
        context['payu_information'] = {}
        context['is_teacher'] = False
        context['show_alert'] = True

        return context


class FeedbackIndex(generic.ListView):
    model = Feedback
    template_name = 'users/feedback_index.html'
    paginate_by = 9
    context_object_name = 'feedbacks'


def social_registration(request):
    if request.method == 'POST':
        user_id = request.POST.get('userID')
        access_token = request.POST.get('accessToken')
        social_type = request.POST.get('socialType')
        user_type = request.POST.get('userType')

        if social_type == 'facebook':
            url_data = 'https://graph.facebook.com/%s?fields=id,first_name,last_name,email,birthday,name,picture{url}&access_token=%s' % (user_id, access_token)

            facebook_request = requests.get(url_data)
            response = facebook_request.json()

            if response.get('error'):
                messages.error(request, 'Error conectando con facebook, por favor intenta nuevamente.')
                return HttpResponseRedirect(reverse('student_signup'))

            user = User.objects.filter(username=user_id).first()

            if not user:
                password = signer_password(user_id)
                form = UserCreationForm({'username': user_id, 'password1': password, 'password2': password})

                if form.is_valid():
                    user = form.save(commit=False)
                    user.first_name = response.get('first_name')
                    user.last_name = response.get('last_name')
                    user.email = response.get('email')
                    user.save()

                    if user_type == 'student':
                        student = Student()
                        photo = urllib.request.urlretrieve(response.get('picture').get('data').get('url'))

                        student.user = user
                        student.photo.save(os.path.basename('%s.jpeg' % user_id), File(open(photo[0], 'rb')))

                        if response.get('birthday'):
                            format_bday = time.strptime(response.get('birthday'), '%m/%d/%Y')
                            student.birth_date = '%s-%s-%s' % (format_bday.tm_year, format_bday.tm_mon, format_bday.tm_mday)

                        student.save()
                        try:
                            subcriber = Subscriber.objects.get(email__iexact=user.email)
                        except ObjectDoesNotExist:
                            Subscriber.objects.create(email=user.email)
                else:
                    messages.error(request, form.errors)
                    return HttpResponseRedirect(reverse('student_signup'))

            login(request, user)
            return redirect(reverse_lazy('users:search_teacher'))

    return redirect(reverse_lazy('student_signup'))


def social_login(request):
    if request.method == 'POST':
        user_id = request.POST.get('userID')
        access_token = request.POST.get('accessToken')
        social_type = request.POST.get('socialType')

        if social_type == 'facebook':
            url_data = 'https://graph.facebook.com/%s?fields=id,first_name,last_name,email,birthday,name,picture{url}&access_token=%s' % (user_id, access_token)

            facebook_request = requests.get(url_data)
            response = facebook_request.json()

            if response.get('error'):
                messages.error(request, 'Error conectando con facebook, por favor intenta nuevamente.')
                return HttpResponseRedirect(reverse('auth_login'))

            user = authenticate(username=user_id, password=signer_password(user_id))

            if user is not None and user.is_active:
                login(request, user)
                return redirect(reverse_lazy('users:search_teacher'))
            else:
                messages.error(request, 'Error de autenticación, por favor intenta nuevamente.')
                return HttpResponseRedirect(reverse('auth_login'))

    return redirect(reverse_lazy('auth_login'))


@csrf_exempt
def subscribe(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        accept = get_boolean_from_request(request, 'accept_tc')

        if not accept:
            return JsonResponse({
                'message': 'Los términos y condiciones no han sido aceptados',
                'success': False
            })

        try:
            exists = Subscriber.objects.get(email=email)
            context = {
                'message': 'El correo {} ya se encuentra registrado'.format(email),
                'success': False
            }
        except Subscriber.DoesNotExist:
            if validateEmail(email):
                instance = Subscriber(email=email)
                instance.save()
                context = {
                    'message': 'Correo registrado correctamente',
                    'success': True
                }

                email_data = {
                    'subscriber': instance,
                    'request': request,
                    'site_name': get_current_site(request).name
                }

                tuto_utils.send_templated_email(
                    subject='Nuevo suscriptor',
                    recipients=','.join(map(str, settings.EMAIL_CONTACT_TO)),
                    template_path='theme/email/new_subscriber.html',
                    data=email_data
                )
            else:
                context = {
                    'message': 'Correo inválido',
                    'success': False
                }

        return JsonResponse(context)

    return HttpResponseNotAllowed('Operación no permitida')


def validateEmail(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError

    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def loadSubjects(request):
    area_id = request.GET.get('area')
    subjects = Subject.objects.filter(area_id=area_id).order_by('name') if area_id else None
    return render(request, 'users/subjects_dropdown_list.html', {'subjects': subjects})


def signer_password(key):
    signer = Signer()
    password = signer.sign(str(key)).split(':')[1]
    return password


def get_request_meta(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return {
        'remoteAddress': ip,
        'userAgent': request.META.get('HTTP_USER_AGENT', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36')
    }


def get_boolean_from_request(request, key, method='POST'):
    " gets the value from request and returns it's boolean state "
    value = getattr(request, method).get(key, False)

    return False if (value == 'False' or value == 'false' or value == '0' or value == 0) else True


def checkTeacherAviability(date_time, duration, teacher):
    start_datetime = datetime.strptime(date_time, '%Y-%m-%d %I:%M %p')
    ending_datetime = start_datetime + timedelta(hours=duration)
    reservations = Reservation.objects.filter(Q(start_datetime_class__range=(start_datetime, ending_datetime)) | Q(end_datetime_class__range=(start_datetime, ending_datetime)), teacher=teacher, status__in=['Pendiente', 'Transacción aprobada'])

    return reservations


class SubjectIndex(generic.ListView):
    model = Subject
    context_object_name = 'subjects'
    template_name = 'users/subject_index.html'

    def get_queryset(self):
        return Subject.objects.filter(visible=True, active=True)
