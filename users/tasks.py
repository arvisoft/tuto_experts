from background_task import background
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
import logging

from .models import Reservation
from theme.models import GeneralSetting
from tuto_experts import utils as tuto_utils

logger = logging.getLogger(__name__)


@background()
def SendStudentReservationConfirmEmail(reservation_id, scheme, host, site_name):
    try:
        from datetime import timedelta
        reservation = Reservation.objects.get(pk=reservation_id)

        try:
            bcc_list = GeneralSetting.objects.get(key__exact='bcc_reservation_list')
        except GeneralSetting.DoesNotExist:
            bcc_list = None

        data = {
            'reservation': reservation,
            'site_name': site_name,
            'host': host,
            'scheme': scheme,
            'is_confirmation': True
        }

        sent = tuto_utils.send_templated_email(
            subject='Confirmación de reserva',
            recipients=reservation.student.email,
            template_path='theme/email/student_reservation_notification.html',
            data=data,
            hidden_recipients=bcc_list
        )

        if not isinstance(sent, str):
            print('Email enviado correctamente')
        else:
            raise Exception(sent)
    except Reservation.DoesNotExist:
        logger.error('Error obteniendo la reserva para enviar el e-mail de confirmacion, id de reserva={0}'.format(reservation_id))
    except Exception as ex:
        logger.error(ex.msg)
