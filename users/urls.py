from django.conf.urls import url

from . import views

app_name = 'users'

urlpatterns = [
    url(r'^subscribe/', views.subscribe, name='subscribe'),
    url(r'^profile/(?P<pk>\d+)/$', views.UserProfileView.as_view(), name='user_profile'),
    url(r'^profile/teacher/(?P<pk>\d+)/edit/$', views.TeacherProfileUpdateView.as_view(), name='teacher_update_profile'),
    url(r'^profile/student/(?P<pk>\d+)/edit/$', views.StudentProfileUpdateView.as_view(), name='student_update_profile'),
    url(r'^search-teacher/', views.SearchTeacherView.as_view(), name='search_teacher'),
    url(r'^teacher-results/', views.TeacherResultView.as_view(), name='teacher_result'),
    url(r'^ajax/load-subjects/', views.loadSubjects, name='ajax_load_subject'),
    url(r'^teacher/(?P<pk>\d+)$', views.TeacherDetailView.as_view(), name='teacher_detail'),
    url(r'^reservation/(?P<pk>\d+)$', views.ReservationDetailView.as_view(), name='reservation')
]
