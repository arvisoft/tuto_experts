import os
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.contrib.humanize.templatetags.humanize import intcomma
from django.db.models import Count, Sum, Min, Max, Value, DateTimeField
from django.db.models.functions import Concat, Trunc
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.contrib import messages
from openpyxl import load_workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import Alignment
from datetime import datetime

from .models import *
from .forms import PaymentForm

# Register your models here.


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    list_display = ['email', 'is_active', 'created_at']
    list_filter = ['created_at']
    list_editable = ['is_active']
    search_fields = ['email']
    list_per_page = 15


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    model = Student
    list_display = ('usuario', 'nombres', 'email', 'birth_date', 'phone', 'preferred_subject', 'activo')
    list_filter = ('user__is_active', )
    list_per_page = 15

    def nombres(self, obj):
        return obj.user.get_full_name()

    def email(self, obj):
        return obj.user.email

    def usuario(self, obj):
        return obj.user.username

    def activo(self, obj):
        return "SI" if obj.user.is_active else "NO"

    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            return ('user',)

        return []


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'email', 'phone', 'work_mode', 'rating', 'activo')
    list_filter = ('work_mode', 'rating', 'user__is_active')
    list_editable = ('work_mode', )
    list_per_page = 15

    def full_name(self, obj):
        return obj.user.get_full_name()
    full_name.short_description = 'Nombres'

    def email(self, obj):
        return obj.user.email
    email.short_description = 'Email'

    def activo(self, obj):
        return "SI" if obj.user.is_active else "NO"

    def get_readonly_fields(self, request, obj=None):
        if obj is None:
            return ('rating', )

        return ('user', 'id_number', 'skype_user', 'rating', 'photo', 'juditial_past', 'certificate')


@admin.register(Area)
class AreaAdmin(admin.ModelAdmin):
    list_display = ('name', 'level', 'formatted_min_price', 'formatted_max_price', 'formatted_increment', 'active')
    list_editable = ('level', 'active')
    list_filter = ('name', 'level')
    list_per_page = 15

    def formatted_min_price(self, obj):
        price = round(float(obj.min_price), 2)
        return "$%s%s" % (intcomma(int(price)), ("%0.2f" % price)[-3:])
    formatted_min_price.short_description = 'Precio Min'

    def formatted_max_price(self, obj):
        price = round(float(obj.max_price), 2)
        return "$%s%s" % (intcomma(int(price)), ("%0.2f" % price)[-3:])
    formatted_max_price.short_description = 'Precio Máx'

    def formatted_increment(self, obj):
        price = round(float(obj.increment), 2)
        return "$%s%s" % (intcomma(int(price)), ("%0.2f" % price)[-3:])
    formatted_increment.short_description = 'Incremento'


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'area', 'active', 'visible', 'image')
    list_editable = ('area', 'active', 'visible', 'image')
    list_filter = ('area', 'active', 'name')
    search_fields = ('name',)
    list_per_page = 15


# @admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'area', 'subject', 'formatted_price', 'active')
    list_editable = ('active',)
    list_per_page = 15

    def formatted_price(self, obj):
        price = round(float(obj.price), 2)
        return "$%s%s" % (intcomma(int(price)), ("%0.2f" % price)[-3:])
    formatted_price.short_description = 'Precio'

    def full_name(self, obj):
        return obj.teacher.get_full_name()
    full_name.short_description = 'Docente'

    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            return ('teacher', 'area', 'subject', 'price')

        return []


# @admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'day', 'start_time', 'end_time')
    list_per_page = 15

    def full_name(self, obj):
        return obj.teacher.get_full_name()
    full_name.short_description = 'Docente'

    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            return ('teacher',)

        return []


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = ('id', 'created_at', 'status')
    list_filter = ('created_at', 'status')
    change_list_template = 'admin/payments_change_list.html'
    list_per_page = 15
    actions = None
    form = PaymentForm

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        # if obj is not None and obj.status == 1:
        #    return True
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            return ('user', 'status')

        return []

    def save_model(self, request, obj, form, change):
        error = False

        if obj is not None:
            excel_file = request.FILES.get('confirmation_file', None)
            try:
                wb = load_workbook(filename=excel_file)
                ws = wb.active

                if not ws['A2'].value:
                    messages.error(request, 'Error en el formato del archivo, los datos no están correctamente digitados.')
                    error = True

                reservations = obj.reservation_set.all()  # reservas asociadas al pago
                rs_by_teacher = reservations.values('teacher__teacher__id_number').annotate(**{'times': Count('teacher')}).order_by('-times')
                excel_data = ws['A2':'B{0}'.format(str(1 + rs_by_teacher.count()))]
                teacher = None

                for row in excel_data:
                    for cell in row:
                        if cell.col_idx == 1:  # columna A
                            teacher = Teacher.objects.get(id_number__iexact=cell.value)
                        else:
                            reservations.filter(teacher=teacher.user).update(payment_proof=cell.value)

                wb.close()
                reservations.update(paid=True)
                obj.status = 2
            except Teacher.DoesNotExist:
                error = True
                messages.error(request, 'Uno de los docentes no fue encontrado.')
            except Exception as ex:
                print(ex)
                error = True
                messages.error(request, 'Error al procesar el arcihvo.')

        if error:
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

        super(PaymentAdmin, self).save_model(request, obj, form, change)

    def get_urls(self):
        urls = super(PaymentAdmin, self).get_urls()

        my_urls = [
            url(r'download-payment-confirmation-template/$', self.download_payment_confirmation_template),
            url(r'generate-payment-file/$', self.generate_payments_file)
        ]

        return my_urls + urls

    def download_payment_confirmation_template(self, request):
        excel_file = load_workbook(settings.STATIC_ROOT + '/documents/payments_confirmation_template.xlsx')
        virtual_excel = save_virtual_workbook(excel_file)
        excel_file.close()

        response = HttpResponse(virtual_excel, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8')
        response['Content-Disposition'] = 'attachment; filename=formato_reporte_de_pagos.xlsx'
        return response

    def generate_payments_file(self, request):
        user = request.user
        if user.is_authenticated() and user.has_perm('users.change_reservation'):
            rsv_not_paid = Reservation.objects.filter(paid=False, confirmed=True)
            rsv_pending = rsv_not_paid.filter(payment__isnull=False)
            payment = None

            if rsv_not_paid and not rsv_pending:
                try:
                    payment = Payment.objects.create(user=request.user, status=1)
                    metrics = {'sales': Sum('amount_to_paid')}
                    excel_info = rsv_not_paid.values(
                        id_number=Concat('teacher__teacher__id_number', Value('')),
                        first_name=Concat('teacher__first_name', Value('')),
                        last_name=Concat('teacher__last_name', Value('')),
                        phone=Concat('teacher__teacher__phone', Value('')),
                    ).annotate(**metrics).order_by('-sales')

                    # Excel File
                    workbook = load_workbook(settings.STATIC_ROOT + '/documents/payments_template.xlsx')
                    worksheet = workbook.active
                    alignment = Alignment(horizontal='center', vertical='center')

                    for row_index, data in enumerate(excel_info):
                        cells = worksheet['A{0}'.format(str(10 + row_index)):'E{0}'.format(str(10 + row_index))][0]

                        for cell in cells:
                            cell.alignment = alignment

                            if cell.col_idx == 1:
                                cell.value = data.get('id_number', '')
                            if cell.col_idx == 2:
                                cell.value = data.get('first_name', '')
                            if cell.col_idx == 3:
                                cell.value = data.get('last_name', '')
                            if cell.col_idx == 4:
                                cell.value = data.get('phone', '')
                            if cell.col_idx == 5:  # columna del valor a pagar 'E'
                                cell.value = data.get('sales', 0)
                                cell.number_format = '$ #,##0.00'

                    worksheet['A3'].value = payment.pk
                    now = datetime.now()
                    virtual_excel = save_virtual_workbook(workbook)
                    workbook.close()

                    response = HttpResponse(virtual_excel, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8')
                    response['Content-Disposition'] = 'attachment; filename=reservas_por_pagar_%s.xlsx' % now.strftime('%Y_%m_%d_%I_%M')

                    rsv_not_paid.update(payment=payment)
                    return response
                except Exception:
                    if payment:
                        payment.delete()
                    messages.error(request, 'Se presentó un error generando el archivo de pagos, por favor intente nuevamente')

            if not rsv_not_paid:
                messages.warning(request, 'No existen reservas pendientes por pagar')

            if rsv_pending:
                messages.warning(request, 'Existen reservas sin confirmar su pago, si ya realizó el pago por favor confirme e intente nuevamente')

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

        return HttpResponseForbidden()


def get_next_in_date_hierarchy(request, date_hierarchy):
    if date_hierarchy + '__day' in request.GET:
        return 'día'
    if date_hierarchy + '__month' in request.GET:
        return 'mes'
    if date_hierarchy + '__year' in request.GET:
        return 'año'
    return 'todos los tiempos'


@admin.register(ReservationSummary)
class ReservationAdmin(admin.ModelAdmin):
    actions = None
    show_full_result_count = False
    date_hierarchy = 'created_at'
    change_list_template = 'admin/reservation_summary_change_list.html'
    list_filter = ('paid', 'teacher__first_name', 'subject__area', 'subject')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return True

    def has_module_permission(self, request):
        return True

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context=extra_context)

        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        metrics = {'total': Count('id'), 'teacher_sales': Sum('amount_to_paid'), 'total_sales': Sum('price')}

        response.context_data['summary'] = list(
            qs
            .values(teacher_full_name=Concat('teacher__first_name', Value(' '), 'teacher__last_name'))
            .annotate(**metrics)
            .order_by('-teacher_sales')
        )

        summary_total = dict(qs.aggregate(**metrics))
        response.context_data['summary_total'] = summary_total

        # Chart
        period = get_next_in_date_hierarchy(request, self.date_hierarchy)
        response.context_data['period'] = period

        summary_over_time = qs.annotate(
            period=Trunc('created_at', 'day', output_field=DateTimeField()),
        ).values('period').annotate(total=Sum('price')).order_by('period')

        summary_range = summary_over_time.aggregate(
            low=Min('total'),
            high=Max('total'),
        )

        high = summary_range.get('high', 0)
        low = summary_range.get('low', 0)
        total_sales = summary_total.get('total_sales', 0)

        response.context_data['summary_over_time'] = [{
            'period': x['period'],
            'total': x['total'] or 0,
            'pct': ((x['total'] or 0) / total_sales) * 100 if total_sales > 0 else 0,
        } for x in summary_over_time]

        return response
