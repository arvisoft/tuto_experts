from django.db.models import Count, Min, Sum, Avg


def update_teacher_rank(sender, instance, created, **kwargs):
    rank = instance
    user = rank.teacher
    rank_average = user.ranks.aggregate(average=Avg('value'))

    if rank_average.get('average') is not None:
        user.teacher.rating = rank_average.get('average')
        user.teacher.save(force_update=True)
