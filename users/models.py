import hashlib
import json
import re
import uuid
from datetime import timedelta

import requests
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from registration.signals import user_registered
from unidecode import unidecode


from .signals import *

# Create your models here.
LEVEL_CHOICES = (
    (1, 'Ed. Básica'),
    (2, 'Ed. Media'),
    (3, 'Ed. Superior'),
    (4, 'Idiomas')
)

CLASS_MODE_CHOICES = (
    (1, 'Presencial'),
    (2, 'Virtual'),
    (3, 'Ambos')
)

CLASS_MODE_CHOICES_FOR_SEARCH = (
    (1, 'Presencial'),
    (2, 'Virtual')
)

DAYS_OF_WEEK_CHOICES = (
    (1, 'Lunes'),
    (2, 'Martes'),
    (3, 'Miercoles'),
    (4, 'Jueves'),
    (5, 'Viernes'),
    (6, 'Sabado'),
    (7, 'Domingo')
)

PSE_CUSTOMER_TYPE = (
    ('N', 'PERSONA NATURAL'),
    ('J', 'PERSONA JURIDICA')
)

PSE_CUSTOMER_ID_TYPE = (
    ('CC', ' CÉDULA DE CUIDADANÍA'),
    ('CE', 'CÉDULA DE EXTRANJERÍA'),
    ('NIT', 'NÚMERO DE IDENTIFICACIÓN TRIBUTARIO'),
    ('TI', 'TARJETA DE IDENTIDAD'),
    ('PP', 'PASAPORTE'),
    ('IDC', 'IDENTIFICADOR ÚNICO DE CLIENTE (ID ÚNICO DE SERVICIO PÚBLICO)'),
    ('CEL', 'IDENTIFICADOR POR LÍNEA MÓVIL'),
    ('RC', 'REGISTRO CIVIL DE NACIMIENTO'),
    ('DE', 'DOCUMENTO DE INDETIFICACIÓN EXTRANJERO')
)

PAYMENT_STATUS_CHOICES = (
    (1, 'GENERADO'),
    (2, 'PAGADO')
)


def profile_photo_folder(instance, filename):
    return '/'.join(['user', 'photos', str(instance.user.id), re.sub('[^0-9a-zA-Z._]+', '', unidecode(filename))])


class Student(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Usuario')
    birth_date = models.DateField(verbose_name='Fecha de nacimiento', auto_now=False, auto_now_add=False, blank=True, null=True)
    photo = models.ImageField(verbose_name='Foto', upload_to=profile_photo_folder, blank=True, null=True)
    phone = models.CharField(verbose_name='Teléfono', blank=True, null=True, max_length=50)
    preferred_subject = models.CharField(verbose_name='Materia de interés', blank=True, null=True, max_length=255)

    class Meta:
        verbose_name = 'Estudiante'
        verbose_name_plural = 'Estudiantes'

    def __str__(self):
        return self.user.email


class Teacher(models.Model):

    def attachment_folder(self, filename):
        return '/'.join(['teacher', 'attachments', str(self.user.id), re.sub('[^0-9a-zA-Z._]+', '', unidecode(filename))])

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Usuario')
    career = models.CharField(blank=False, null=False, max_length=150, verbose_name='Profesión')
    biography = models.TextField(verbose_name='Acerca de', blank=True, null=True)
    phone = models.BigIntegerField(verbose_name='Teléfono', blank=True, null=True)
    id_number = models.BigIntegerField(verbose_name='Identificación', blank=False, null=False, db_index=True, help_text='Número de identificación', unique=True)
    rating = models.FloatField(default=0)
    work_mode = models.IntegerField(verbose_name='Modo de trabajo', choices=CLASS_MODE_CHOICES)
    skype_user = models.CharField(max_length=50, verbose_name='Skype')
    facebook_link = models.URLField(verbose_name='Facebook', blank=True, null=True)
    instagram_link = models.URLField(verbose_name='Instagram', blank=True, null=True)
    twitter_link = models.URLField(verbose_name='Twitter', blank=True, null=True)
    linkedin_link = models.URLField(verbose_name='LinkedIn', blank=True, null=True)
    google_link = models.URLField(verbose_name='Google+', blank=True, null=True)
    photo = models.ImageField(verbose_name='Foto', upload_to=profile_photo_folder, blank=True, null=True)
    juditial_past = models.FileField(verbose_name='Antecedentes judiciales', upload_to=attachment_folder, blank=True, null=True)
    certificate = models.FileField(verbose_name='Certificado pedagógico', upload_to=attachment_folder, blank=True, null=True)

    class Meta:
        verbose_name = 'Docente'
        verbose_name_plural = 'Docentes'

    def __str__(self):
        return self.user.get_full_name()


class Subscriber(models.Model):
    email = models.EmailField(unique=True, db_index=True)
    is_active = models.BooleanField(default=True, verbose_name='Activo')
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, editable=False, verbose_name='Creado')

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Suscriptor'
        verbose_name_plural = 'Suscriptores'

    def __str__(self):
        return self.email


class Area(models.Model):
    name = models.CharField(max_length=200, blank=False, verbose_name='Nombre')
    level = models.IntegerField(choices=LEVEL_CHOICES, blank=False, verbose_name='Nivel')
    description = models.TextField(verbose_name='Descripción', blank=True, null=True)
    min_price = models.IntegerField(blank=False, verbose_name='Precio Min')
    max_price = models.IntegerField(blank=False, verbose_name='Precio Max')
    increment = models.IntegerField(blank=False, verbose_name='Incremento', default=1000)
    active = models.BooleanField(default=True, verbose_name='Activo')

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Subject(models.Model):
    def images_folder(self, filename):
        return '/'.join(['subject', 'images', str(self.id), re.sub('[^0-9a-zA-Z._]+', '', unidecode(filename))])

    area = models.ForeignKey(Area, related_name='area_subjects', verbose_name='Area', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, verbose_name='Nombre')
    description = models.TextField(verbose_name='Descripción', blank=True, null=True)
    image = models.ImageField(verbose_name='Imagen', blank=True, null=True, upload_to=images_folder)
    active = models.BooleanField(default=True, verbose_name='Activo')
    visible = models.BooleanField(default=False, verbose_name='Visible', help_text='Visible en la sección de materias')

    class Meta:
        verbose_name = 'Materia'
        verbose_name_plural = 'Materias'
        ordering = ['name']

    def __str__(self):
        return self.name


# Clase para almacenar la informacion de los pagos a los docentes (archivos de excel generados para pagar por parte de tuto experts)
class Payment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user', on_delete=models.SET_NULL, null=True)
    status = models.IntegerField(verbose_name='Estado', choices=PAYMENT_STATUS_CHOICES)
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True, editable=False, verbose_name='Fecha')

    def __str__(self):
        return '%s, %s' % (str(self.pk), self.created_at.strftime('%A %d %B, %Y %I:%M %p'))

    class Meta:
        verbose_name = 'Pago'
        verbose_name_plural = 'Pagos'


class Reservation(models.Model):
    student = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='student_reservations', verbose_name='Estudiante', on_delete=models.CASCADE)
    teacher = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='teacher_reservations', verbose_name='Docente', on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, related_name='subject_reservations', verbose_name='Materia', on_delete=models.CASCADE)
    topic = models.TextField(verbose_name='Tema')
    duration = models.IntegerField(verbose_name='Duración')
    start_datetime_class = models.DateTimeField(verbose_name='Fecha y Hora Inicio')
    end_datetime_class = models.DateTimeField(verbose_name='Fecha y Hora Fin')
    mode = models.IntegerField(choices=CLASS_MODE_CHOICES, verbose_name='Modalidad')
    price = models.FloatField(verbose_name='Valor total')
    amount_to_paid = models.FloatField(default=0)
    paid = models.BooleanField(default=False, help_text='Pagada al docente', verbose_name='Pagado a docente')
    address = models.TextField(blank=True, null=True)
    edited_at = models.DateTimeField(auto_now_add=False, auto_now=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, editable=False)
    payment = models.ForeignKey(Payment, on_delete=models.DO_NOTHING, blank=True, null=True)
    payment_proof = models.CharField(blank=True, null=True, max_length=50, verbose_name='Número de transacción', help_text='Referencia de pago (cuando se pague al docente)')
    confirmed = models.BooleanField(db_index=True, default=False, verbose_name='Confirmado por Estudiante')
    # Campos de payu
    reference_code = models.CharField(max_length=50, verbose_name='Código de referencia', blank=True, null=True)
    order_id = models.BigIntegerField(verbose_name='Orden Payu', db_index=True, help_text='Número de orden generado por Payu', default=1)
    transaction_id = models.CharField(max_length=50, verbose_name='No Transacción', db_index=True, default='')
    status = models.CharField(max_length=100, verbose_name='Estado', db_index=True, default='Pendiente')
    trazability_code = models.CharField(max_length=50, verbose_name='Código de Trazabilidad', db_index=True, default='')
    authorization_code = models.CharField(max_length=50, verbose_name='Código de Autorización', blank=True, null=True)
    url_payment_pdf = models.URLField(verbose_name='PDF', blank=True, null=True, help_text='Solo aplica para pagos en efectivo')
    url_payment_web = models.URLField(verbose_name='WEB', blank=True, null=True, help_text='Solo aplica para pagos en efectivo')
    reference_number = models.BigIntegerField(verbose_name='No Referencia', blank=True, null=True, help_text='Solo aplica para pagos en efectivo')

    def __str__(self):
        return self.topic

    class Meta:
        ordering = ['created_at']
        verbose_name = 'Reserva'
        verbose_name_plural = 'Reservas'

    def save(self, *args, **kwargs):
        self.amount_to_paid = self.price - ((self.price * settings.SITE_GAIN) + settings.SITE_GAIN)
        self.end_datetime_class = self.start_datetime_class + timedelta(hours=self.duration)
        super(Reservation, self).save(*args, **kwargs)

    def get_payu_information(self):
        data = {
            'test': False,
            'language': 'es',
            'command': 'ORDER_DETAIL_BY_REFERENCE_CODE',
            'merchant': {
                'apiKey': settings.PAYU_API_KEY,
                'apiLogin': settings.PAYU_API_LOGIN
            },
            'details': {
                'referenceCode': self.reference_code
            }
        }
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8'
        }
        request = requests.post('https://api.payulatam.com/reports-api/4.0/service.cgi', data=json.dumps(data), headers=headers, timeout=60)
        response = request.json()

        print(response)
        return {
            'paymentMethod': response.get('result').get('payload')[0].get('transactions')[0].get('paymentMethod'),
            'buyer': response.get('result').get('payload')[0].get('buyer'),
            'payer': response.get('result').get('payload')[0].get('transactions')[0].get('payer'),
            'bankName': response.get('result').get('payload')[0].get('transactions')[0].get('extraParameters').get('FINANCIAL_INSTITUTION_NAME')
        }


class Skill(models.Model):
    teacher = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='skills', verbose_name='Usuario', on_delete=models.CASCADE)
    area = models.ForeignKey(Area, related_name='skill_areas', verbose_name='Area', on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, related_name='skill_subjects', verbose_name='Materia', on_delete=models.CASCADE)
    price = models.IntegerField(verbose_name='Precio')
    edited_at = models.DateTimeField(auto_now_add=False, auto_now=True, editable=False, verbose_name='Editado')
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, editable=False, verbose_name='Creado')
    active = models.BooleanField(default=True, verbose_name='Activo')

    def __str__(self):
        return self.teacher.get_full_name()

    class Meta:
        verbose_name = 'Habilidad'
        verbose_name_plural = 'Habilidades laborales'


class Schedule(models.Model):
    teacher = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='schedules', verbose_name='Docente', on_delete=models.CASCADE)
    day = models.IntegerField(choices=DAYS_OF_WEEK_CHOICES, verbose_name='Día')
    start_time = models.TimeField(verbose_name='Inicio', auto_now=False, auto_now_add=False, blank=True, null=True)
    end_time = models.TimeField(verbose_name='Fin', auto_now=False, auto_now_add=False, blank=True, null=True)

    def __str__(self):
        return self.teacher.get_full_name()

    class Meta:
        verbose_name = 'Horario'
        verbose_name_plural = 'Horarios'


class Rank(models.Model):
    teacher = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='ranks', verbose_name=_("teacher"), on_delete=models.CASCADE)
    value = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, editable=False)

    class Meta:
        verbose_name = 'Puntuación'
        verbose_name_plural = 'Puntuaciones'

    def __str__(self):
        return self.teacher.get_full_name()


class Feedback(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='feedbacks', on_delete=models.CASCADE)
    rating = models.IntegerField(default=1, verbose_name='Puntuación')
    text = models.TextField(verbose_name='Texto')
    user_type = models.PositiveSmallIntegerField(help_text='1 para docente, 0 para estudiantes', default=0)
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, editable=False)

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'

    def __str__(self):
        return self.user.get_full_name()

    def save(self, *args, **kwargs):
        try:
            self.user.teacher
            self.user_type = 1
        except ObjectDoesNotExist:
            self.user_type = 0
        super(Feedback, self).save(*args, **kwargs)


def user_registered_callback(sender, user, request, **kwargs):
    user_type = request.POST.get('user_type', 'student')

    if user_type == 'student':
        phone = request.POST.get('phone_number', None)
        pref_subject = request.POST.get('preferred_subject', None)
        student = Student(user=user, phone=phone, preferred_subject=pref_subject)
        student.save()

    try:
        subcriber = Subscriber.objects.get(email__iexact=user.email)
    except ObjectDoesNotExist:
        Subscriber.objects.create(email=user.email)


user_registered.connect(user_registered_callback)
post_save.connect(update_teacher_rank, sender=Rank)


# Clase para armar las estructuras que se deben enviar al API de Payu para realizar los pagos
class Payu(models.Model):

    payment_method = models.CharField(max_length=50, blank=True, null=True)
    base_price = models.FloatField()
    description = models.TextField()
    client_ip = models.CharField(max_length=25, blank=True, null=True)
    user_agent = models.TextField(blank=True, null=True)
    device_session_id = models.CharField(blank=True, null=True, max_length=255)
    # Datos del comprador
    student_id = models.IntegerField(blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    # PSE
    bank_id = models.IntegerField(blank=True, null=True)
    # Tarjetas de crédito
    cc_number = models.BigIntegerField(blank=True, null=True)
    cc_expiration = models.CharField(blank=True, null=True, max_length=50)
    cc_verfication_code = models.IntegerField(blank=True, null=True)
    # Datos del pagador
    payer_name = models.CharField(max_length=255, blank=True, null=True)
    payer_phone = models.CharField(max_length=255, blank=True, null=True)
    payer_type = models.CharField(max_length=5, blank=True, null=True, choices=PSE_CUSTOMER_TYPE)
    payer_id_type = models.CharField(max_length=5, blank=True, null=True, choices=PSE_CUSTOMER_ID_TYPE)
    payer_id = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False

    def __str__(self):
        return 'Payu Object'

    def get_reference_code(self):
        return uuid.uuid4().hex.upper()

    def get_tax_value(self):  # IVA configured in .env file
        return self.base_price * settings.TAX_BASE

    def get_tx_value(self):  # Valor total
        return self.base_price + self.get_tax_value()

    def get_signature(self, reference_code):
        base_string = "%s~%s~%s~%s~COP" % (settings.PAYU_API_KEY, settings.PAYU_MERCHANT_ID, reference_code, self.get_tx_value())
        hash_string = hashlib.md5(base_string.encode('utf-8'))
        return hash_string.hexdigest()

    def get_cash_payment_json(self):
        baloto = self.get_base_json_request()
        baloto.get('transaction').update({
            'expirationDate': (timezone.now() + timezone.timedelta(days=settings.PAYU_MAX_DAYS_TO_PAY)).strftime('%Y-%m-%dT%H:%M:%S')
        })

        return baloto

    def get_pse_payment_json(self):
        pse = self.get_base_json_request()

        pse.get('transaction').update({
            'payer': {
                'fullName': self.payer_name,
                'contactPhone': self.payer_phone,
                'emailAddress': self.email
            },
            'extraParameters': {
                'FINANCIAL_INSTITUTION_CODE': str(self.bank_id),
                'USER_TYPE': self.payer_type,
                'PSE_REFERENCE1': self.client_ip,
                'PSE_REFERENCE2': self.payer_id_type,
                'PSE_REFERENCE3': self.payer_id
            }
        })

        return pse

    def get_cc_payment_json(self):
        cc_json = self.get_base_json_request()

        if settings.DEBUG:
            cc_json.update({
                'test': True
            })

        cc_json.get('transaction').update({
            'payer': {
                'fullName': self.payer_name,
                'dniNumber': self.payer_id,
                'emailAddress': self.email,
                'contactPhone': self.phone,
                'billingAddress': {
                    'street1': self.address,
                    'city': self.city,
                    'phone': self.phone,
                    'country': 'CO'
                }
            },
            'creditCard': {
                'number': self.cc_number,
                'securityCode': self.cc_verfication_code,
                'expirationDate': self.cc_expiration,
                'name': self.payer_name
            },
            'extraParameters': {
                'INSTALLMENTS_NUMBER': 1
            }
        })

        return cc_json

    def get_ping_json(self):
        return {
            'test': False,
            'language': 'es',
            'command': 'PING',
            'merchant': {
                'apiLogin': settings.PAYU_API_LOGIN,
                'apiKey': settings.PAYU_API_KEY
            }
        }

    def get_bank_list_json(self):
        return {
            'language': 'es',
            'command': 'GET_BANKS_LIST',
            'merchant': {
                'apiKey': settings.PAYU_API_KEY,
                'apiLogin': settings.PAYU_API_LOGIN
            },
            'test': False,
            'bankListInformation': {
                'paymentMethod': 'PSE',
                'paymentCountry': 'CO'
            }
        }

    def get_base_json_request(self):
        reference_code = self.get_reference_code()

        return {
            'language': 'es',
            'command': 'SUBMIT_TRANSACTION',
            'merchant': {
                'apiKey': settings.PAYU_API_KEY,
                'apiLogin': settings.PAYU_API_LOGIN
            },
            'transaction': {
                'order': {
                    'accountId': settings.PAYU_ACCOUNT_ID,
                    'referenceCode': reference_code,
                    'description': self.description,
                    'language': 'es',
                    'signature': self.get_signature(reference_code=reference_code),
                    'additionalValues': {
                        'TX_VALUE': {
                            'value': self.get_tx_value(),
                            'currency': 'COP'
                        },
                        'TX_TAX': {
                            'value': self.get_tax_value(),
                            'currency': 'COP'
                        },
                        'TX_TAX_RETURN_BASE': {
                            'value': self.base_price,
                            'currency': 'COP'
                        }
                    },
                    'buyer': {
                        'merchantBuyerId': self.student_id,
                        'fullName': self.full_name,
                        'emailAddress': self.email,
                        'contactPhone': self.phone,
                        'shippingAddress': {
                            'street1': self.address,
                            'city': self.city,
                            'phone': self.phone,
                            'country': 'CO'
                        }
                    }
                },
                'type': 'AUTHORIZATION_AND_CAPTURE',
                'paymentMethod': self.payment_method,
                'paymentCountry': 'CO',
                'ipAddress': self.client_ip,
                'userAgent': self.user_agent,
                'deviceSessionId': self.device_session_id
            },
            'test': True
        }


class ReservationSummary(Reservation):
    class Meta:
        proxy = True
        verbose_name = 'Resumen de Reserva'
        verbose_name_plural = 'Resumen de Reservas'
