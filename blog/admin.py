from django.contrib import admin
from .models import Post, Comment, Category
from .forms import PostAdminForm
# Register your models here.


class PostAdmin(admin.ModelAdmin):
    form = PostAdminForm
    list_editable = ('category',)
    list_filter = ('category',)
    search_fields = ('title',)
    list_display = ('title', 'comment_count', 'category')

    class Media:
        css = {
            'all': ('css/medical.css',)
        }


admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
admin.site.register(Category)
