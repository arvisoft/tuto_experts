from django.conf.urls import url

from . import views

app_name = 'blog'

urlpatterns = [
    url(r'^$', views.BlogIndex.as_view(), name='index'),
    url(r'^create/$', views.BlogCreate.as_view(), name='create'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<slug>[-_\w]+)/$', views.BlogDetail.as_view(), name='detail')
]
