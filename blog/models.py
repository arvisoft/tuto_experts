import re
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.template import defaultfilters
from django.db.models.signals import post_save
from redactor.fields import RedactorField
from unidecode import unidecode
from datetime import datetime


from .signals import save_comment


class Category(models.Model):
    name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True, verbose_name='Activo')

    def __str__(self):
        return self.name

    class Meta():
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorías'


class Post(models.Model):

    def attachment_folder(self, filename):
        now = datetime.now()
        return '/'.join(['blog/uploads/', now.strftime('%Y/%m/%d'), re.sub('[^0-9a-zA-Z._]+', '', unidecode(filename))])

    title = models.CharField(max_length=200, verbose_name='Titulo')
    slug = models.SlugField()
    body_text = RedactorField(verbose_name='Texto', upload_to=attachment_folder, redactor_options={'lang': 'es', 'focus': True})
    post_date = models.DateTimeField(auto_now_add=True, verbose_name='Fecha Registro')
    modified = models.DateTimeField(null=True, verbose_name='Fecha Modificación')
    posted_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, verbose_name='Autor', on_delete=models.SET_NULL)
    allow_comments = models.BooleanField(default=True, verbose_name='Permitir Comentarios')
    comment_count = models.IntegerField(blank=True, default=0, verbose_name='Comentarios')
    featured_image = models.ImageField(upload_to=attachment_folder, blank=True, null=True, verbose_name='Imagen')
    category = models.ForeignKey(Category, null=True, related_name='category', verbose_name='Categoría', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'post'
        verbose_name_plural = 'posts'
        ordering = ['-post_date']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug,
            'year': '%04d' % self.post_date.year,
            'month': '%02d' % self.post_date.month,
            'day': '%02d' % self.post_date.day,
        }

        return reverse('blog:detail', kwargs=kwargs)

    def save(self, *args, **kwargs):
        slug = defaultfilters.slugify(self.title[:45])
        same_slug = Post.objects.filter(slug__startswith=slug)

        if same_slug:
            slug = defaultfilters.slugify(self.title[:45] + ' ' + str(same_slug.count()))

        self.slug = slug
        super(Post, self).save(*args, **kwargs)


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comments', verbose_name='Post', on_delete=models.CASCADE)
    body_text = models.TextField(verbose_name='Mensaje')
    post_date = models.DateTimeField(auto_now_add=True, verbose_name='Fecha')
    ip_address = models.GenericIPAddressField(default='0.0.0.0', verbose_name='IP')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, verbose_name='Usuario', related_name='comment_user', on_delete=models.SET_NULL)
    user_name = models.CharField(max_length=50, default='anonymous', verbose_name='Nombre de Usuario')
    user_email = models.EmailField(blank=True, verbose_name='Correo')

    def __str__(self):
        return self.body_text

    class Meta:
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'
        ordering = ['-post_date']


post_save.connect(save_comment, sender=Comment)
