from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import generic
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormMixin
from .models import Post, Comment
from .forms import CommentForm

# Create your views here.


class BlogIndex(generic.ListView):
    template_name = 'blog/index.html'
    context_object_name = 'posts'
    paginate_by = 5
    queryset = Post.objects.all()


class BlogDetail(FormMixin, generic.DetailView):
    template_name = 'blog/detail.html'
    model = Post
    form_class = CommentForm

    def get_success_url(self):
        kwargs = {
            'slug': self.object.slug,
            'year': '%04d' % self.object.post_date.year,
            'month': '%02d' % self.object.post_date.month,
            'day': '%02d' % self.object.post_date.day,
        }
        return reverse('blog:detail', kwargs=kwargs)

    def get_context_data(self, **kwargs):
        context = super(BlogDetail, self).get_context_data(**kwargs)
        context['form'] = CommentForm(initial={'post': self.object, 'user': self.request.user})
        context['related_posts'] = Post.objects.filter(category=self.object.category).exclude(pk=self.object.pk)[0:4]
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        form.save()
        return super(BlogDetail, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class BlogCreate(generic.CreateView):
    template_name = 'blog/create_blog.html'
    model = Post
    fields = ['title', 'body_text', 'allow_comments', 'featured_image', 'category']

    def form_valid(self, form):
        post = form.save()
        post.posted_by = self.request.user
        post.save()
        return redirect('blog:index')
