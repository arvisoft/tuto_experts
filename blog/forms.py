from django import forms
from redactor.widgets import RedactorEditor
from .models import Comment, Post


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['body_text', 'post', 'user']
        widgets = {
            'post': forms.HiddenInput(),
            'user': forms.HiddenInput(),
            'body_text': forms.Textarea(attrs={'class': 'sm-form-control', 'cols': '58', 'rows': '7', 'tabindex': '4'})
        }


class PostAdminForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'category', 'allow_comments', 'featured_image', 'body_text')
        widgets = {
            'body_text': RedactorEditor()
        }
