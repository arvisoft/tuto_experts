function enable_disable_related_input(chk, data_filter) {
    $('[data-' + data_filter + '-for=' + chk.value + ']').attr('disabled', !chk.checked)
}

$(document).ready(function () {
    var skills = context_skills.replace(/&#39;/g, '"'),
        schedule = context_schedule.replace(/&#39;/g, '"').replace(/datetime.time/g, '').replace(/\(/g, '"').replace(/\)/g, '"'),
        chk_subjects = $('[name=subjects]'),
        subject_prices = $('.subject_price'),
        chk_schedule = $('[name=days]')

    subject_prices.attr('disabled', true)

    $('input[name=schedule_start] , input[name=schedule_end]').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        dynamic: false,
        dropdown: true,
        scrollbar: false
    })

    // cargar los valores que ya tiene
    JSON.parse(skills).forEach(skill => {
        var combo = $('[data-subject-for=' + skill.subject + ']')[0],
            chk = $(chk_subjects).filter('[value=' + skill.subject + ']')[0]

        $(chk).prop('checked', true)
        $(combo).prop('disabled', false).val(skill.price)
    })

    // cargar los valores que ya tiene
    var today = moment().format('YYYY-MM-DD ')
    JSON.parse(schedule).forEach(data => {
        var startTime = moment(data.start_time, 'h, mm').format('hh:mm A'),
            endTime = moment(data.end_time, 'h, mm').format('hh:mm A'),
            startInput = $('[data-day-start-for=' + data.day + ']')[0],
            endInput = $('[data-day-end-for=' + data.day + ']')[0],
            chk = $(chk_schedule).filter('[value=' + data.day + ']')[0]

        $(chk).prop('checked', true)
        $(startInput).attr('disabled', false).val(startTime)
        $(endInput).attr('disabled', false).val(endTime)
    })

    $("form").submit(function () {

        if ($('input:checkbox[name="subjects"]').filter(':checked').length == 0) {
            showBottomDialog('warning', 'Por favor seleccione al menos una materia!')
            return false
        }

        var selected_days = $('input:checkbox[name="days"]').filter(':checked')

        if (selected_days.length == 0) {
            showBottomDialog('warning', 'Por favor seleccione al menos un día de disponibilidad!')
            return false
        } else {
            var schedule_start_time = $('input[name=schedule_start]'),
                schedule_end_time = $('input[name=schedule_end]'),
                all_ok = true

            selected_days.each(function (index, element) {
                var day = parseInt($(element).val()),
                    start_time = $(schedule_start_time[day - 1]).val(),
                    end_time = $(schedule_end_time[day - 1]).val()

                if (!start_time) {
                    showBottomDialog('warning', 'No ha seleccionado hora de inicio de atención para el día ' + $(element).attr('data-day-name'))
                    all_ok = false
                    return false
                }

                if (!end_time) {
                    showBottomDialog('warning', 'No ha seleccionado hora de finalización de atención para el día ' + $(element).attr('data-day-name'))
                    all_ok = false
                    return false
                }


                if (start_time == end_time) {
                    showBottomDialog('warning', 'Seleccione un horario de atención válido para el día ' + $(element).attr('data-day-name'))
                    all_ok = false
                    return false
                }
            })

            return all_ok
        }
    });
});