$(function () {
    window.fbAsyncInit = function () {
        FB.init({
            appId: fcb_app_id,
            cookie: true,
            xfbml: true,
            version: fcb_api_v
        });

        FB.AppEvents.logPageView();

    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

})

function facebook_login(form) {
    if (form == 'form-social-register') fbq('track', 'CompleteRegistration');

    FB.login(function (response) {
        if (response.status == 'connected') {
            $('input[name="userID"]').val(response.authResponse.userID)
            $('input[name="accessToken"]').val(response.authResponse.accessToken)
            $('input[name="socialType"]').val('facebook')

            $('#' + form).submit()
            return
        }

        showBottomDialog('warning', '<i class="icon-warning-sign"></i> No se pudo completar la operación con Facebook.');
    }, {
        scope: 'user_birthday,email,public_profile'
    })
}

function register_student() {
    fbq('track', 'CompleteRegistration');
    // gtag_report_conversion(`${location.href}accounts/register/student/`);
    $('#register-student-form').submit()
}

register_student_home = () => {
    var form = $('#register-student-form'),
        submitBtn = $('#btn-submit'),
        validForm = true

    $.each($(form).serializeArray(), (_, field) => {
        if (!['phone_number', 'preferred_subject'].includes(field.name) && !field.value) {
            $(`input[name="${field.name}"]`).addClass('error')
            validForm = false
        }
    });

    if (!isValidEmailAddress($('input[name=email]').val())) {
        validForm = false
        $('input[name=email]').addClass('error')
        return
    }

    if (!isValidEmailAddress($('input[name=email]').val())) {
        validForm = false
        $('input[name=email]').addClass('error')
        return
    }

    if ($('input[name=password1]').val() !== $('input[name=password2]').val()) {
        validForm = false
        $('input[name=password1]').addClass('error')
        $('input[name=password2]').addClass('error')
        return
    }

    if (!$('input[name=terms_and_conditions]').is(':checked')) {
        validForm = false
        showBottomDialog('warning', '<i class=icon-warning-sign></i> <b>Debes aceptar los términos y condicioness</b>');
        return
    }

    $(submitBtn).prop('disabled', true)
    $(submitBtn).html('<i class="icon-line-loader icon-spin nomargin"></i>')

    if (validForm) {
        $.post(form.attr('action'), form.serialize())
            .done((response) => {
                try {
                    fbq('track', 'CompleteRegistration');
                    // gtag_report_conversion(`${location.href}accounts/register/student/`);
                } catch (error) {}
                window.location.replace(response.data)
            })
            .fail((response) => {
                try {
                    const data = response.responseJSON.data
                    var errors = $('#form-error')

                    $('div').remove('.sign-up-errors')
                    $(errors).removeClass('hidden')
                    Object.values(data).forEach(element => {
                        element.forEach(err_msg => {
                            $(errors).append(`
                            <div class="sign-up-errors">
                                <i class="icon-chevron-sign-right"></i> ${err_msg}
                            </div>
                            `)
                        });
                    });
                } catch (error) {
                    showBottomDialog('warning', '<i class=icon-warning-sign></i> <b>No pudimos procesar tu solicitud, por favor intenta nuevamente</b>');
                }
            })
            .always((response) => {
                $(submitBtn).prop('disabled', false)
                $(submitBtn).html('Enviar')
            })
    }
}