$(function () {
    $('.cc_month').datepicker({
        format: 'mm',
        language: 'es',
        autoclose: true,
        minViewMode: 1
    })

    $('.cc_year').datepicker({
        format: 'yyyy',
        language: 'es',
        autoclose: true,
        minViewMode: 2,
        startDate: new Date()
    })

    $('.timepicker').timepicker({
        timeFormat: 'hh:mm p',
        dynamic: false,
        dropdown: true,
        scrollbar: false
    })
})

/** credit card validations */
$('#id_cc_number').keypress(function (event) {
    return (event.charCode >= 46 && event.charCode <= 57) || event.charCode == 8
})

$('#id_cc_verification_code').keypress(function (event) {
    return (event.charCode >= 46 && event.charCode <= 57) || event.charCode == 8
})

$('form').submit(function () {
    var active_payment_method = $('.accordion').find('.acc_content').filter(function () {
            return this.style.display != 'none'
        }),
        payment_method = $(active_payment_method).attr('data-payment-method')

    if (payment_method == 'PSE') {
        var titular_name = $('input[name=pse_payer_name]').val().trim(),
            id_number = $('input[name=pse_payer_id]').val().trim(),
            phone_number = $('input[name=pse_payer_phone]').val().trim()

        if (titular_name.length <= 0 || id_number.length <= 0 || phone_number.length <= 0) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> Por favor digite todos los campos para realizar el pago con PSE.');
            return false
        }
    }

    if (payment_method == 'TC') {
        var cc_titular_name = $('input[name=cc_titular]').val().trim(),
            cc_titular_id = $('input[name=cc_titular_id]').val().trim(),
            cc_number = $('input[name=cc_number]').val().trim(),
            cc_valid_code = $('input[name=cc_verification_code]').val().trim(),
            cc_expiration_month = $('input[name=cc_expiration_month]').val().trim(),
            cc_expiration_year = $('input[name=cc_expiration_year]').val().trim()

        if (cc_titular_name.length <= 0 || cc_number.length <= 0 || cc_valid_code.length <= 0 || cc_expiration_month.length <= 0 || cc_expiration_year <= 0 || cc_titular_id <= 0) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> Por favor digite todos los campos para realizar el pago con su tarjeta.');
            return false
        }

        if (cc_valid_code.length > 4) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> El código de verificación no puede tener más de 4 dígitos.')
            return false
        }

        var only_number = /^\d+$/,
            valid_card = /^\d{13,16}$/
        // el numero de la tarjeta y el codigo de verificacion deben ser numericos
        if (!only_number.test(cc_number) || !only_number.test(cc_valid_code)) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> El número de tarjeta y código de verificación deben ser númericos.');
            return false
        }

        if (!valid_card.test(cc_number)) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> Número de tarjeta inválido, por favor rectifique.');
            return false
        }

        // validar la fecha de caducidad
        if (!only_number.test(cc_expiration_month) || !only_number.test(cc_expiration_year)) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> La fecha de caducidad no es válida.');
            return false
        }

        var now = new Date()
        if (Number(cc_expiration_year) < now.getFullYear() || (Number(cc_expiration_month) < now.getMonth() + 1 && Number(cc_expiration_year) == now.getFullYear())) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> La fecha de caducidad no puede ser menor a la fecha actual.');
            return false
        }

        // nombre del titular solo puede tener letras
        var only_letters = /^[a-zA-Z\s]+$/
        if (!only_letters.test(cc_titular_name)) {
            showBottomDialog('warning', '<i class="icon-warning-sign"></i> El nombre del titular debe contener solo letras (sin acentos) y espacios.');
            return false
        }
    }


    $('<input />').attr('type', 'hidden').attr('name', 'payment_method').attr('value', payment_method).appendTo('form')
    return true
})