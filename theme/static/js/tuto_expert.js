function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

function showSubscriptionTC() {
    const mail = $('#widget-subscribe-form-email')

    if (!mail.val() || !isValidEmailAddress(mail.val())) return showBottomDialog('warning', '<i class=icon-warning-sign></i> <b>Por favor digita un email válido</b>');
    else return $('#subscriptionModal').modal('toggle')
}

function sendSubscription() {
    const form = $('#widget-subscribe-form'),
        chk_tc = $('#checkbox-subscribe'),
        accept_tc = $(chk_tc).is(':checked')

    if (!accept_tc) return showBottomDialog('warning', '<i class="fa-icon-warning"></i> <b>Los términos y condiciones no han sido aceptados</b>')

    $(form).append($(chk_tc).val(accept_tc ? true : false))
    $.post(form.attr('action'), form.serialize(), function (data) {
        const message = data.success ? '<i class=icon-ok-sign></i> <b>' + data.message + '</b>' : '<i class=icon-warning-sign></i> <b>' + data.message + '</b>'

        showBottomDialog(data.success ? 'success' : 'warning', message)
        if (data.success) {
            form.resetForm()
            return $('#subscriptionModal').modal('toggle')
        }
    })
}

function sendContact() {
    var form = $('#widget-contact-form'),
        name = $('#widget-contact-form-name'),
        email = $('#widget-contact-form-email'),
        message = $('#widget-contact-form-message'),
        validForm = true;


    if (!name.val()) name.addClass('error'), validForm = false;
    if (!email.val() || !isValidEmailAddress(email.val())) email.addClass('error'), validForm = false;
    if (!message.val()) message.addClass('error'), validForm = false;
    if (!validForm) return;

    $('.form-process').show();

    $.ajax({
        url: form.attr('action'),
        data: form.serialize(),
        type: form.attr('method'),
        success: function (response) {
            $('.form-process').hide();
            var message = response.success ? '<i class=icon-ok-sign></i> <b>' + response.message + '</b>' : '<i class=icon-warning-sign></i> <b>' + response.message + '</b>';
            showBottomDialog(response.success ? 'success' : 'warning', message);
            if (response.success) form.resetForm();
        },
        error: function (xhr, text, error) {
            $('.form-process').hide();
            showBottomDialog('error', error || 'Error interno en el servidor');
        }
    });
}

function showBottomDialog(type, message) {
    el = document.createElement('a');
    el.dataset.notifyType = type;
    el.dataset.notifyMsg = message;
    el.dataset.notifyPosition = "bottom-full-width";
    el.style.cssText = 'left:-9999px;top:-9999px;';
    document.body.appendChild(el);
    SEMICOLON.widget.notifications(el)
    document.body.removeChild(el);
}

jQuery(document).ready(function ($) {
    var $faqItems = $('#faqs .faq');
    if (window.location.hash == '') {
        var getFaqFilterHash = '#cat-1';
        var hashFaqFilter = getFaqFilterHash.split('#');

        if ($faqItems.hasClass(hashFaqFilter[1])) {
            $('#faqs-filter li').removeClass('activeFilter');
            $('[data-filter=".' + hashFaqFilter[1] + '"]').parent('li').addClass('activeFilter');
            var hashFaqSelector = '.' + hashFaqFilter[1];
            $faqItems.css('display', 'none');
            if (hashFaqSelector != 'all') {
                $(hashFaqSelector).fadeIn(500);
            } else {
                $faqItems.fadeIn(500);
            }
        }
    }

    $('#faqs-filter a').click(function () {
        $('#faqs-filter li').removeClass('activeFilter');
        $(this).parent('li').addClass('activeFilter');
        var faqSelector = $(this).attr('data-filter');
        $faqItems.css('display', 'none');
        if (faqSelector != 'all') {
            $(faqSelector).fadeIn(500);
        } else {
            $faqItems.fadeIn(500);
        }
        return false;
    });
});