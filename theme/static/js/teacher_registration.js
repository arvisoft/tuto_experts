function enable_disable_related_input(chk, data_filter) {
    $('[data-' + data_filter + '-for=' + chk.value + ']').attr('disabled', !chk.checked)
}

$(document).ready(function () {
    $('input[name=schedule_start] , input[name=schedule_end]').timepicker({
        timeFormat: 'hh:mm p',
        interval: 30,
        dynamic: false,
        dropdown: true,
        scrollbar: false
    })

    $("form").submit(function () {

        if (!$('[name=terms_and_conditions]').is(':checked')) {
            showBottomDialog('warning', 'Debe aceptar los términos y condiciones.')
            return false
        }

        if ($('input:checkbox[name="subjects"]').filter(':checked').length == 0) {
            showBottomDialog('warning', '¡Por favor seleccione al menos una materia!')
            return false
        }

        const selected_days = $('input:checkbox[name="days"]').filter(':checked')

        if (selected_days.length == 0) {
            showBottomDialog('warning', '¡Por favor seleccione al menos un día de disponibilidad!')
            return false
        } else {
            var schedule_start_time = $('input[name=schedule_start]'),
                schedule_end_time = $('input[name=schedule_end]'),
                all_ok = true

            selected_days.each(function (index, element) {
                var day = parseInt($(element).val()),
                    start_time = $(schedule_start_time[day - 1]).val(),
                    end_time = $(schedule_end_time[day - 1]).val()

                if (!start_time) {
                    showBottomDialog('warning', 'No ha seleccionado hora de inicio de atención para el día ' + $(element).attr('data-day-name'))
                    all_ok = false
                    return false
                }

                if (!end_time) {
                    showBottomDialog('warning', 'No ha seleccionado hora de finalización de atención para el día ' + $(element).attr('data-day-name'))
                    all_ok = false
                    return false
                }


                if (start_time == end_time) {
                    showBottomDialog('warning', 'Seleccione un horario de atención válido para el día ' + $(element).attr('data-day-name'))
                    all_ok = false
                    return false
                }
            })

            try {
                // if (all_ok) gtag_report_conversion(`${location.href}accounts/register/teacher/`);
            } catch (error) {}
            return all_ok
        }
    })
});