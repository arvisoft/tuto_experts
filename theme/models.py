from django.db import models

# Create your models here.

FAQS_CATEGORIES_CHOICES = (
    (1, 'Para estudiantes y padres de familia'),
    (2, 'Para docentes expertos')
)


class Faq(models.Model):
    title = models.CharField(max_length=150, blank=False, null=False, verbose_name='Pregunta')
    description = models.TextField(blank=False, null=False, verbose_name='Respuesta')
    active = models.BooleanField(default=True, verbose_name='Visible')
    category = models.IntegerField(default=1, blank=False, choices=FAQS_CATEGORIES_CHOICES, verbose_name='Categoría')
    created_at = models.DateTimeField(auto_now_add=True, auto_now=False, editable=False, verbose_name='Creado')

    class Meta:
        verbose_name = 'Pregunta Frecuente'
        verbose_name_plural = 'Preguntas Frecuentes'
        ordering = ['category', 'created_at']
        indexes = [
            models.Index(fields=['title'], name='faq_title_idx')
        ]

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title


class GeneralSetting(models.Model):
    key = models.CharField(max_length=150, verbose_name='Llave', db_index=True, unique=True)
    value = models.CharField(verbose_name='Valor', max_length=255)
    description = models.CharField(verbose_name='Descripción', max_length=255)

    class Meta:
        verbose_name = 'Configuración General'
        verbose_name_plural = 'Configuraciones Generales'
        indexes = (
            models.Index(fields=['key'], name='general_setting_idx'),
        )

    def __str__(self):
        return self.description

    def __unicode__(self):
        return self.description
