from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views import generic

from tuto_experts import utils as tuto_utils
from users.forms import StudentSignUpForm
from users.models import Feedback, Reservation, Student, Subscriber, Teacher

from .forms import ContactForm
from .models import Faq

# Create your views here.


def index(request):
    if request.method == 'POST':
        form = StudentSignUpForm(request.POST)
        if form.is_valid():
            form.save_from_home(request=request)
            response = {
                'message': 'operación exitosa',
                'data': request.scheme + '://' + request.META["HTTP_HOST"] + reverse("registration_complete")
            }
            return JsonResponse(response, status=200)
        else:
            response = {
                'message': 'Por favor complete los datos',
                'data': form.errors
            }
            return JsonResponse(response, status=400)
    else:
        feedbacks = Feedback.objects.all()
        form = StudentSignUpForm()

        context = {
            'form': form,
            'subscribers': Subscriber.objects.filter(is_active=True).count(),
            'teachers': Teacher.objects.filter(user__is_active=True).count(),
            'students': Student.objects.filter(user__is_active=True).count(),
            'reservations': Reservation.objects.filter(status='Transacción aprobada').count(),
            'featured_teachers': Teacher.objects.filter(user__is_active=True).order_by('-rating')[:6],
            'students_feedback': feedbacks.filter(user_type=0)[:2],
            'teachers_feedback': feedbacks.filter(user_type=1)[:2],
        }

    if request.user.is_authenticated() and reverse('auth_login') in request.META.get('HTTP_REFERER'):
        try:
            request.user.student
            return redirect(reverse('users:search_teacher'))
        except ObjectDoesNotExist:
            pass

    return render(request, 'theme/index.html', context)


def contact(request):
    if request.method == 'POST':
        contact_form = ContactForm(request.POST or None)
        response = {
            'message': 'Por favor complete los datos',
            'success': False
        }

        if contact_form.is_valid():
            subject = contact_form.cleaned_data.get('subject')

            if not subject:
                subject = 'Nuevo mensaje'

            data = {
                'email': contact_form.cleaned_data.get('sender_email'),
                'full_name': contact_form.cleaned_data.get('sender_full_name'),
                'subject': subject,
                'message': contact_form.cleaned_data.get('message'),
                'phone': contact_form.cleaned_data.get('sender_phone'),
                'city': contact_form.cleaned_data.get('sender_city'),
                'site_name': get_current_site(request).name,
                'request': request
            }

            try:
                sent = tuto_utils.send_templated_email(
                    subject='Nuevo mensaje de contacto',
                    recipients=','.join(map(str, settings.EMAIL_CONTACT_TO)),
                    template_path='theme/email/contact_email.html',
                    data=data
                )

                if not isinstance(sent, str):
                    response = {
                        'success': True,
                        'message': '!Gracias! Hemos recibido tus comentarios.'
                    }
                else:
                    raise Exception(sent)
            except Exception:
                response = {
                    'success': False,
                    'message': 'No hemos podido recibir tus comentarios, por favor intenta nuevamete.'
                }

        return JsonResponse(response)
    else:
        contact_form = ContactForm()

    return render(request, 'theme/contact.html', {'contact_form': contact_form})


def AboutUs(request):
    return render(request, 'theme/about.html', {})


def TermsAndConditions(request):
    return render(request, 'theme/terms_conditions.html', {})


def ParticularClasses(request):
    return render(request, 'theme/particular_classes.html', {})


class FaqsIndex(generic.ListView):
    model = Faq
    template_name = 'theme/faqs.html'
    context_object_name = 'faqlist'

    def get_queryset(self):
        return Faq.objects.filter(active=True)

    def get_context_data(self, **kwargs):
        context = super(FaqsIndex, self).get_context_data(**kwargs)
        return context
