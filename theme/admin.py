from django.contrib import admin

from .models import Faq, GeneralSetting
# Register your models here.


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ['title', 'active', 'category']
    list_editable = ['active', 'category']
    list_filter = ['active', 'category']
    search_fields = ['title']


@admin.register(GeneralSetting)
class GeneralSettingAdmin(admin.ModelAdmin):
    actions = None
    list_display = ('description', 'key', 'value')
    list_editable = ('value',)
    search_fields = ('description',)

    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            return ('key',)

        return []

    def has_delete_permission(self, request, obj=None):
        return False
