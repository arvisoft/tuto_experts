from django import forms


class ContactForm(forms.Form):
    sender_full_name = forms.CharField(
        max_length=150, required=True, help_text='Máximo 150 carácteres', label='Nombres',
        error_messages={'required': 'Por favor digita tu nombre', 'max_length': 'Máximo 150 carácteres'},
        widget=forms.TextInput(attrs={'class': 'sm-form-control required', 'id': 'widget-contact-form-name'}))
    sender_email = forms.EmailField(
        required=True, error_messages={'required': 'Por favor digita un correo'}, label='Correo',
        widget=forms.TextInput(attrs={'class': 'sm-form-control required email', 'id': 'widget-contact-form-email'}))
    sender_phone = forms.CharField(
        max_length=20, required=False, label='Teléfono',
        widget=forms.TextInput(attrs={'class': 'sm-form-control required'}))
    sender_city = forms.CharField(
        max_length=20, required=True, label='Ciudad',
        widget=forms.TextInput(attrs={'class': 'sm-form-control required'}))
    subject = forms.CharField(
        max_length=150, required=False, label='Asunto',
        widget=forms.TextInput(attrs={'class': 'sm-form-control'}))
    message = forms.CharField(
        required=True, label='Mensaje',
        widget=forms.Textarea(attrs={'class': 'sm-form-control required', 'id': 'widget-contact-form-message'}),
        error_messages={'required': 'No no has dicno nada, por favor escribe tu mensaje'})
