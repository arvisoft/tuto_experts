import os
from django.conf import settings
from email.mime.image import MIMEImage
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.template import TemplateDoesNotExist


def get_logo_for_email():
    logo_path = '{0}/images/logo-tuto-experts.png'.format(settings.STATIC_ROOT)
    path, filename = os.path.split(logo_path)
    logo = open(logo_path, 'rb')
    msg_logo = MIMEImage(logo.read())
    logo.close()
    msg_logo.add_header('Content-ID', '<{0}>'.format(filename))

    return msg_logo


def send_templated_email(subject, recipients, template_path, data, hidden_recipients=None):
    '''
    Envia un correo con el contenido del template dado

    subject String: Titulo del mensaje

    recipients String: Lista de destinatarios separados por coma (,)

    template_path String: Ubicación del template

    data dic: datos para pasar al template

    hidden_recipients String: lista de destinatarios ocultos separados por coma (,), default None

    Return True if operation succeeded or String.
    '''

    try:
        body_msg = render_to_string(template_path, data)

        email = EmailMessage(
            from_email='Tuto Experts <{}>'.format(settings.EMAIL_HOST_USER),
            subject=subject,
            to=[recipients],
            body=body_msg
        )

        if hidden_recipients:
            email.bcc = [hidden_recipients]

        email.content_subtype = 'html'
        email.mixed_subtype = 'related'
        email.attach(get_logo_for_email())
        email.send()

        return True
    except TemplateDoesNotExist as ex:
        return str(ex.msg)
