"""tuto_experts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from registration.backends.default.views import RegistrationView

from theme import views
from users import forms, views as userViews

admin.site.te_header = "Tuto Experts"
admin.site.site_title = "Administrador Tuto Experts"
admin.site.index_title = "Bienvenido"


urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^checkout/', userViews.CheckoutView.as_view(), name='checkout'),
    url(r'^payment_response/', userViews.PaymentResponseView.as_view(), name='payment_response'),
    url(r'^payment_confirmation/', userViews.payment_confirmation, name='payment_confirmation'),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^faqs/', views.FaqsIndex.as_view(), name='faqs'),
    url(r'^feedback/', userViews.FeedbackIndex.as_view(), name='feedback'),
    url(r'^about-us/', views.AboutUs, name='about'),
    url(r'^subjects/', userViews.SubjectIndex.as_view(), name='subjects'),
    url(r'^terms-and-conditions/', views.TermsAndConditions, name='terms_and_conditions'),
    url(r'^users/', include('users.urls')),
    url(r'^accounts/register/student/', RegistrationView.as_view(form_class=forms.StudentSignUpForm), name='student_signup'),
    url(r'^accounts/register/teacher/', userViews.TeacherRegistrationView.as_view(), name='teacher_signup'),
    url(r'^accounts/social/register/', userViews.social_registration, name='social_register'),
    url(r'^accounts/social/login/', userViews.social_login, name='social_login'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^blog/', include('blog.urls')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^config/', admin.site.urls),
    url(r'^particular-classes/', views.ParticularClasses, name='particular_classes'),
    url(r'^reservation-confirmation/(?P<reservation_code>\w+)/$', userViews.ReservationConfirmationView.as_view(), name='reservation-confirmation'),
    url(r'^reservation-detail/(?P<reservation_code>\w+)/$', userViews.ReservationResumeDetailView.as_view(), name='reservation-detail')
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
