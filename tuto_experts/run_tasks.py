import os

# venv_dir = '/home/marrieta/Documents/Projects/Python/venv/'
venv_dir = '/home/manearriefonc/Documents/Python36Dev/'
activate_script = os.path.join(venv_dir, 'bin', 'activate_this.py')

# execfile(activate_script, dict(__file__=activate_script))
exec(compile(open(activate_script, "rb").read(), activate_script, 'exec'), dict(__file__=activate_script))

# os.system('python manage.py process_tasks --duration 15')
os.system('python manage.py process_tasks')
